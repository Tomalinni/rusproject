**Необходимы установленные Node.js, typescript, Angular CLI, gulp, bower**

### Сборка приложения для разработки

1. Установите URL вашего localhost API
	В файле [root]/tools/build.json установите в параметр config.path.demoApiUrl URL вашего localhost API	
	(на данный момент установлен "demoApiUrl": "http://localhost:4200/")

2. Запустите терминал и измените вашу текущую директорию на папку tools проекта, в которой расположены файлы сборки приложения. 
		```cd tools```
		
3. Обновите исправленное API URL
		```gulp apiurl```
		
4. Запустите приложение:
	Из директории src/app запустите команду
		```ng serve -o```

	
##### Компиляция зависимостей (после внесения изменений в scss, js скомпилирует файлы styles.bundle.css и scripts.bundle.js):
	Из директории tools запустите команду
		gulp
	
##### Запуск вотчера:
	gulp watch
	
##### Запуск вотчера для scss или js:
	gulp watch:js
	gulp watch:scss

	
### Компиляция приложения для продакшена

1. Подключить минифицированные файлы CSS и JS, если они доступны
	Из директории tools запустите команду
		```gulp --prod```

2. Собрать приложение
	Из директории src/app запустите команду
		```ng build --prod```