import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'counter-rows',
  templateUrl: './counter-rows.component.html'
})
export class CounterRowsComponent implements OnInit {
  rowFrom: number = 1;
  rowUntil: number;
  dtLength: number;
  filteredLength: number;
  isFiltered: string;
  datatable: any;

  constructor() { }

  ngOnInit() {
  }

  /*
  countRows(event) {
    this.rowFrom = (event.first == 0) ? 1 : event.first + 1;
    this.dtLength = this.datatable.value.length;
    let _rowUntil = this.rowFrom + event.rows - 1;

    if (_rowUntil > this.dtLength) {
      return this.rowUntil = this.dtLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  countRowsFilter(event) {
    this.rowFrom = 1;
    let _rowUntil = this.rowFrom + this.datatable.rows - 1;
    this.dtLength = this.datatable.value.length;
    this.rowUntil = event.filteredValue.length;
    this.filteredLength = event.filteredValue.length;

    if (this.filteredLength == 0) {
      this.isFiltered = 'empty';
    } else if (this.filteredLength == 1) {
      this.isFiltered = 'one';
    } else if (this.dtLength == this.filteredLength) {
      this.isFiltered = '';
    } else {
      this.isFiltered = 'fromTo';
    }

    if (_rowUntil > this.filteredLength) {
      return this.rowUntil = this.filteredLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  getDt(el) {
    return this.datatable = el;
  }*/

}
