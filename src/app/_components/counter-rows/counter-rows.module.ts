import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CounterRowsComponent } from "./counter-rows.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CounterRowsComponent],
  exports: [CounterRowsComponent]
})
export class CounterRowsModule { }
