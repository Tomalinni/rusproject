import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'counter-symbols',
  templateUrl: './counter-symbols.component.html'
})
export class CounterSymbolsComponent implements OnInit {
  @Input() maxSymbols: number;
  @Input() currentSymbols: number;

  constructor() { }

  ngOnInit() {
  }

}
