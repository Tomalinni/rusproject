import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CounterSymbolsComponent } from "./counter-symbols.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CounterSymbolsComponent],
  exports: [CounterSymbolsComponent]
})
export class CounterSymbolsModule { }
