import { Injectable, NgModule, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/timer';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from "../auth/_services/authentication.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  refreshTokenInProgress = false;
  tokenRefreshedSource = new Subject();
  tokenRefreshed$ = this.tokenRefreshedSource.asObservable();

  private authService(): AuthenticationService {
    return this.inj.get(AuthenticationService);
  }
  //allows injection of not-yet-initialized service (= resolve circular dependency)
  constructor(private inj: Injector) { }

  refreshToken(): Observable<void> {
    //Первый кто пытается обновить - взводит refreshTokenInProgress; остальные тогда просто ждут tokenRefreshed$
    //  и ничего не делают; потом все они (первый и остальные) повторяются заново.
    //А если первый упал с ошибкой - надо видимо дать им всем упасть с ошибкой.

    if (this.refreshTokenInProgress) {
      return new Observable<void>(observer => {
        this.tokenRefreshed$.subscribe(() => {
          observer.next();
          observer.complete();
        }, (err) => {
          observer.error(err);
          observer.complete();
        });
      });
    } else {
      this.refreshTokenInProgress = true;
      return this.authService().refreshAccessToken()
        //emulate 2 seconds delay in refresh - to get more requests in queue
        //.switchMap(() => Observable.timer(2000).switchMap(() => Observable.of<void>(null)))
        //emulate error
        //.switchMap(() => Observable.throw("Emulate error"))
        .catch((err) => {
          this.tokenRefreshedSource.error(err);
          this.refreshTokenInProgress = false;
          return Observable.throw(err);
        })
        .switchMap(() => {
          this.tokenRefreshedSource.next();
          this.refreshTokenInProgress = false;
          return Observable.of<void>(null);
        });
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.headers.keys().find((x: string) => x.toLowerCase() == "authorization")) {
      req = this.updateAuthorization(req);
    }

    return next.handle(req)
      .catch(err => {
        if (err instanceof HttpErrorResponse && err.status == 401) {
          //          this.refreshToken().subscribe((data) => {
          //            console.log(data);
          //          }, (error) => {
          //            console.error(error);
          //          }); //simulate parallel requests

          //retry the request after refreshing token
          return this
            .refreshToken()
            .switchMap(() => {
              let updated = this.updateAuthorization(req);
              return next.handle(updated);
            })
            .catch(() => {
              this.authService().logout();
              return Observable.throw(err); //re-throw
            });
        } else {
          return Observable.throw(err);
        }
      });
    //todo logout если почему-либо не получилось - тоже тут, централизованно
  }

  private updateAuthorization(req: HttpRequest<any>) {
    let token = sessionStorage.getItem('token');
    if (token) {
      return req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json"
        }
      })
    } else {
      return req.clone({
        setHeaders: {
          Authorization: `None`,
          "Content-Type": "application/json"
        }
      })
    }
  }
}

@NgModule({
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ]
})
export class InterceptorModule { }
