import { Http, URLSearchParams, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Rx';
import { IdentityService } from './identity.service';
import { environment } from '../../environments/environment';

@Injectable()
export class HttpService {
  public beforeRequest;
  public afterRequest;

  constructor(private http: Http, private router: Router, private identityService: IdentityService) {
  }

  public sendGet(url, params?) {
    // this.beforeRequest();
    const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Custom ' + environment.token });
    return this
      .http
      .get(environment.baseUrl + url, { search: this.setParams(params), headers: headers })
      .toPromise();
  }

  //
  // public sendPost(url, body?, params?) {
  //   this.beforeRequest();
  //   return this.addHandlers(this.http.post(this.baseUrl + url, body, {search: this.setParams(params)}));
  // }
  //
  // public longPull(url, params?) {
  //   this.beforeRequest();
  //   const result = Observable.interval(500)
  //     .take(10)
  //     .switchMap(() => this.http.get(this.baseUrl + url, {search: this.setParams(params)}))
  //     .map(this.toObject);
  //   result.subscribe(null, null, this.afterRequest);
  //   return result;
  // }
  //
  // public longPullUnlimit(url, params?) {
  //   return Observable.interval(500)
  //     .switchMap(() => this.http.get(this.baseUrl + url, {search: this.setParams(params)}))
  //     .map(this.toObject);
  // }
  //
  // public toObject(response) {
  //   try {
  //     return response.json();
  //   } catch (ex) {
  //     return response;
  //   }
  // }
  //
  // private addHandlers(request: Observable<Response>) {
  //   const component = this;
  //   return request
  //     .toPromise()
  //     .then(function (response) {
  //       component.afterRequest();
  //       return component.toObject(response)
  //     }, function (error) {
  //       console.log(error);
  //       // component.sendGet('/error', [{key : 'error', value: error.toString()}]);
  //       component.afterRequest();
  //       // component.router.navigate(['/error']);
  //     });
  // }
  //
  private setParams(dictionary) {
    const params: URLSearchParams = new URLSearchParams();
    if (dictionary) {
      dictionary.forEach(function(item) {
        params.set(item.key, item.value);
      });
    }
    return params;
  }
  //
  // private handleError(error: any): Promise<any> {
  //   console.error('An error occurred', error); // for demo purposes only
  //   return Promise.reject(error.message || error);
  // }
}
