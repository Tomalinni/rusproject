import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/toPromise';

@Injectable()



export class IdentityService {

  loginPromise: Promise<any>;
  private token: string;

  constructor(private http: Http) { }

  getToken(): Promise<string> {
    if (this.token) {
      return new Promise((resolve, reject) => resolve(this.token));
    }

    return this.loginForm();
  }

  loginForm(): Promise<string> {
    if (this.loginPromise) {
      return this.loginPromise;
    }
    const service = this;

    this.loginPromise = new Promise((resolve, reject) => {
      const modal = document.getElementById('globalModalLoginForm');
      const btn = document.getElementById('globalModalLoginFormLoginButton');
      const usernameText = <HTMLInputElement>document.getElementById('globalModalLoginFormUsername');
      const passwordText = <HTMLInputElement>document.getElementById('globalModalLoginFormPassword');

      modal.style.display = 'block';

      btn.onclick = function() {
        const login = usernameText.value;
        const password = passwordText.value;
        modal.style.display = 'none';
        passwordText.value = '';
        service.loginUser(login, password)
          .then((token) => {
            modal.style.display = 'none';
            service.token = token;
            resolve(token);
          }, () => {
            modal.style.display = 'block';
            alert('Неправильный логин или пароль');
          });
      };
    });
    return this.loginPromise;
  }

  loginUser(username: string, password: string): Promise<string> {
    let headers: Headers = new Headers();
    headers.append("Authorization", "Basic " + btoa("rajithapp" + ":" + "secret"));
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    //const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Basic' + btoa(login + ":" + password)});
    return this
      .http
      .post(environment.baseUrl + '/oauth/token?username=' + username + '&password=' + password + '&grant_type=password', { login: username, password: password }, { headers: headers })
      .toPromise()
      .then(response => {
        return response.json();
      });
  }



  loginUserTest(login: string, password: string): Promise<string> {
    debugger;
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return new Promise((resolve, reject) => {
      const observ = this.http.post(environment.baseUrl + '/rms-authorization/user/login/', { login: login, password: password }, { headers: headers });
      observ.subscribe(response => {
        // data is now an instance of type ItemsResponse, so you can do this:
        debugger;
        resolve(response.json().headerValue);
      });
    });
  }

}
