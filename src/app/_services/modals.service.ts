import { Injectable } from '@angular/core';

@Injectable()
export class ModalsService {

  contentAttentionPlan: any = {
    text: 'You have not saved changes to the milestone. Are you sure you want to save the plan?',
    button: 'Save plan'
  };

  contentConfirmPlan: any = {
    name: 'Plan',
    event: 'is saved'
  };

  contentConfirmBlockUser: any = {
    name: 'Пользователь',
    event: 'заблокирован'
  };


  contentConfirmUnblockUser: any = {
    text: 'Пользователь успешно разблокирован'
  };


  content: any = {
    confirm: {
      addOrganization: {
        name: 'Организация',
        event: 'создана'
      },
      addAdminOrg: {
        name: 'Пользователь',
        event: 'создан и привязан к текущей организации'
      },
      addFunction: {
        name: 'Function',
        event: 'is created'
      },
      changeFunction: {
        name: 'Function',
        event: 'is changed'
      },
      deleteFunction: {
        name: 'Function',
        event: 'is deleted'
      }
    },
    attention: {
      addOrganization: {
        text: 'Данные не будут сохранены. Вы уверены, что хотите выйти?',
        button: 'Подтвердить выход'
      },
      addAdminOrg: {
        text: 'Вы хотите зарегистрировать администратора клиники?',
        buttonSuccess: 'Да',
        buttonCancel: 'Позже'
      },
      deleteFunction: {
        title: 'Attention',
        description: 'Are you sure you want to delete this function?',
        textOk: 'Yes',
        textCancel: 'No'
      }
    },
    error: {
      addOrganization: {
        name: 'Ошибка на сервере!',
        event: 'Организация не создана! Попробуйте еще раз.'
      }
    }
  }

}
