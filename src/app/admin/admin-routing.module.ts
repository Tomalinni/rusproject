import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "../auth/_guards/auth.guard";

import { AdminComponent } from './admin.component';


const routes: Routes = [
  {
    "path": "",
    "component": AdminComponent,
    "canActivate": [AuthGuard],
    "children": [
      {
        "path": "", redirectTo: 'admin-users', pathMatch: 'full'
      },
      {
        "path": "admin-users",
        "loadChildren": ".\/users\/user-admin\/user-admin.module#UserAdminModule"
      },
      {
        "path": "users",
        "loadChildren": ".\/users\/user\/user.module#UserModule"
      },
      {
        "path": "templates",
        "loadChildren": ".\/templates\/templates.module#TemplatesModule"
      },
      {
        "path": "organisations",
        "loadChildren": ".\/organisations\/organisations.module#OrganisationsModule"
      },
      {
        "path": "functions",
        "loadChildren": ".\/functions\/functions.module#FunctionsModule"
      },
      {
        "path": "doctors",
        "loadChildren": ".\/doctor\/doctor.module#DoctorModule"
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
