import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DropdownModule } from 'primeng/primeng';

import { LayoutModule } from './../theme/layouts/layout.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { ModalComponent } from './modal/modal.component';
import { ModalConfirmComponent } from './modal/modal-confirm/modal-confirm.component';
import { AddNewAdminModel } from "./modal/modal-confirm/addNewAdminModel";
import { ModalAttentionComponent } from './modal/modal-attention/modal-attention.component';
import { ModalDialogComponent } from "./modal/modal-dialog/modal-dialog.component";
import { ModalDefaultAttentionComponent } from './modal/modal-default-attention/modal-default-attention';
import { UsersService } from "./users/users.service";

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
  ],
  declarations: [
    AdminComponent,
    ModalComponent,
    ModalConfirmComponent,
    ModalDialogComponent,
    AddNewAdminModel,
    ModalDefaultAttentionComponent,
    ModalAttentionComponent
  ],
  entryComponents: [
    ModalComponent,
    ModalConfirmComponent,
    ModalDialogComponent,
    AddNewAdminModel,
    ModalDefaultAttentionComponent,
    ModalAttentionComponent
  ]
})
export class AdminModule { }
