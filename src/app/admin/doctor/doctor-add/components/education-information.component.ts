import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { EducationInformationModel } from "../model/education-information.model";
import 'rxjs/add/operator/map';
import { PrimengService } from "../../../../_services/primeng.service";
import { minDate } from "ng2-validation/dist/min-date";


@Component({
  selector: 'doctor-education-information',
  templateUrl: './education-information.component.html',
})
export class EducationInformationComponent implements OnInit {

  @Input() inputData: EducationInformationModel;
  @Output() isValid: EventEmitter<boolean> = new EventEmitter();

  public educationForm: FormGroup;

  classDD: string;
  countryList: any;

  filteredCities: string[];
  cityList: City[];

  filteredEducationalInstitution: string[];
  educationalInstitutionList: EducationalInstitution[];

  filteredProfession: string[];
  professionList: Profession[];

  minDate: Date;
  maxDate: Date;
  ru: any;



  constructor(private _script: ScriptLoaderService,
    private primengService: PrimengService) {

    this.countryList = primengService.countryList;

    this.cityList = [new City('Москва')];
    this.educationalInstitutionList = [new EducationalInstitution('МГУ')];
    this.professionList = [new Profession('Терапевт')];
    this.maxDate = new Date();

    let minDate = new Date();
    minDate.setFullYear(new Date().getFullYear() - 100);
    this.minDate = minDate;


  }

  ngOnInit() {

    this.educationForm = new FormGroup({
      country: new FormControl()
      , city: new FormControl(null, [Validators.required, this.spaceValidation])
      , educationalInstitution: new FormControl(null, [Validators.required, this.spaceValidation])
      , profession: new FormControl(null, [Validators.required, this.spaceValidation])
      , endEducationDate: new FormControl(null, [Validators.required, this.dateRangeValidation])
      , diplomaNumber: new FormControl(null)
    });

    if (this.inputData != undefined) {
      this.educationForm.patchValue(this.inputData);
    }

    this.educationForm.statusChanges.subscribe(data => {
      if (this.educationForm.touched)
        this.isValid.emit(data == "VALID");
    });

    this.ru = this.primengService.ru;
  }

  ngAfterViewInit() {
    this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
      'assets/app/js/components/doctor/doctor-add/education.js');

  }

  getModelDD(el) {
    this.classDD = el;
  }

  filterCities(event) {
    this.filteredCities = [];

    for (let i = 0; i < this.cityList.length; i++) {
      let city = this.cityList[i].name;
      if (city.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filteredCities.push(city);
      }
    }
  }


  filterEducationalInstitution(event) {
    this.filteredEducationalInstitution = [];

    for (let i = 0; i < this.educationalInstitutionList.length; i++) {
      let educationalInstitution = this.educationalInstitutionList[i].name;
      if (educationalInstitution.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filteredEducationalInstitution.push(educationalInstitution);
      }
    }
  }

  filterProfession(event) {
    this.filteredProfession = [];

    for (let i = 0; i < this.professionList.length; i++) {
      let profession = this.professionList[i].name;
      if (profession.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filteredProfession.push(profession);
      }
    }
  }


  public spaceValidation(fControl: FormControl) {
    let isSpace = (fControl.value || '').trim().length === 0;
    let isValid = !isSpace;
    return isValid ? null : { 'spaces': true }
  }

  public dateRangeValidation(fControl: FormControl) {

    let minDate = new Date();
    minDate.setFullYear(new Date().getFullYear() - 100);
    let maxDate = new Date();

    let selecteDate = fControl.value;
    if ((selecteDate < minDate) || (selecteDate > maxDate))
      return { 'datesRange': true }
    else
      return null;
  }

}


export class City {
  constructor(public name: string) { }
}

export class EducationalInstitution {
  constructor(public name: string) { }
}

export class Profession {
  constructor(public name: string) { }
}
