import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild, } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CustomValidators } from 'ng2-validation';
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { MainInformationModel } from "../model/main-information.model";
import { LanguageModel } from "../model/language.model";
import { PrimengService } from "../../../../_services/primeng.service";
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'doctor-main-information',
  templateUrl: './main-information.component.html',
})
export class MainInformationComponent implements OnInit {
  @ViewChild('elMultiSelect') elMultiSelect;

  @Input() inputData: MainInformationModel;
  @Output() isValid: EventEmitter<boolean> = new EventEmitter();

  public mainForm: FormGroup;

  languages: SelectItem[];
  ru: any;
  minDate: Date;
  maxDate: Date;
  countryList: any;
  maskList: any;
  countryPhone: string = 'RF';
  maskPhone: string;
  placeholderMask: string;
  mdlCountryPhone: string;
  mdlSelectedLang: string[] = [];
  classDD: string;


  constructor(
    private _script: ScriptLoaderService,
    private primengService: PrimengService,

  ) {
    this.ru = primengService.ru;
    this.minDate = primengService.setMinDate();
    this.maxDate = primengService.setMaxDate();
    this.countryList = primengService.countryList;
    this.maskList = primengService.maskList;
  }
  openMultiSelect() {
    let event = new MouseEvent('click');
    this.elMultiSelect.el.nativeElement.children["0"].dispatchEvent(event);
  }
  ngOnInit() {

    this.languages = [
      { label: "Русский", value: "Русский" }
      , { label: "Английский", value: "Английский" }
      , { label: "Немецкий", value: "Немецкий" }
    ];

    this.mainForm = new FormGroup({
      firstName: new FormControl()
      , lastName: new FormControl()
      , middleName: new FormControl()
      , sex: new FormControl()
      , birthday: new FormControl('', Validators.compose([Validators.required, CustomValidators.minDate(this.minDate), CustomValidators.maxDate(this.maxDate)]))
      , phoneDoctor: new FormControl()
      , countryPhoneDoctor: new FormControl()
      , email: new FormControl()
      , selectedLang: new FormControl('')
      , сhipsLang: new FormControl()
    });

    if (this.inputData != undefined) {
      this.mainForm.patchValue(this.inputData);
    }

    this.mainForm.statusChanges.subscribe(data => {
      if (this.mainForm.touched)
        this.isValid.emit(data == "VALID")
    });

    this.mdlCountryPhone = this.countryPhone;
    this.maskPhone = this.maskList[this.countryPhone].mask;
    this.placeholderMask = this.maskList[this.countryPhone].placeholder;
  }


  getFieldCountry(fld) {
    let control = fld.name;

    if (control == 'countryPhoneDoctor') {
      this.countryPhone = fld.value;
      this.maskPhone = this.maskList[this.countryPhone].mask;
      this.placeholderMask = this.maskList[this.countryPhone].placeholder;
    }
  }

  ngAfterViewInit() {
  }

}
