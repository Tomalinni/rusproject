import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";

import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { MedicalInformationModel } from "../model/medical-information.model";
import 'rxjs/add/operator/map';


@Component({
  selector: 'doctor-medical-information',
  templateUrl: './medical-information.component.html',
})
export class MedicalInformationComponent implements OnInit {

  @Input() inputData: MedicalInformationModel;
  @Output() isValid: EventEmitter<boolean> = new EventEmitter();

  public medicalForm: FormGroup;

  constructor(private _script: ScriptLoaderService) { }

  ngOnInit() {

    this.medicalForm = new FormGroup({
      fullName: new FormControl('', [Validators.required])
    });

    if (this.inputData != undefined) {
      this.medicalForm.patchValue(this.inputData);
    }

    this.medicalForm.statusChanges.subscribe(data => {
      if (this.medicalForm.touched)
        this.isValid.emit(data == "VALID");
    });

  }

  onChanges(): void {
    console.log("changed");
  }

  ngAfterViewInit() {


  }

}
