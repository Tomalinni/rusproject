import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[dateSymbols]'
})
export class DateDirective {

  private regex: RegExp = new RegExp("[0-9]|[.]");

  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'Ctrl'];

  constructor(private el: ElementRef) {
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {

    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    //let current: string = this.el.nativeElement.value;

    let next: string = event.key;
    if (String(next).match(this.regex) == null) {
      event.preventDefault();
    }
  }
}
