import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[symbolsAndDash]'
})
export class SymbolsAndDashDirective {

  private regex: RegExp = new RegExp("^[\\d_@.#$=!%^)(\\]:\\*;\\?\\/\\,}{'\\|<>\\[&\\+]*$");

  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'Ctrl'];

  constructor(private el: ElementRef) {
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {

    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    //let current: string = this.el.nativeElement.value;

    let next: string = event.key;
    if (String(next).match(this.regex) != null) {
      event.preventDefault();
    }
  }
}
