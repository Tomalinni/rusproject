import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { EducationInformationComponent } from "./components/education-information.component";
import { MedicalInformationComponent } from "./components/medical-information.component";
import { MainInformationComponent } from "./components/main-information.component";
import { CreateDoctorDto } from "./model/create-doctor.dto";

export type Steps = "MAIN" | "EDUCATION" | "MEDICAL" | "NONE";

@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './doctor-add.component.html',
})
export class DoctorAddComponent implements OnInit {

  @ViewChild(EducationInformationComponent) educationInformationComponent: EducationInformationComponent;
  @ViewChild(MainInformationComponent) mainInformationComponent: MainInformationComponent;
  @ViewChild(MedicalInformationComponent) medicalInformationComponent: MedicalInformationComponent;

  constructor(private _cdr: ChangeDetectorRef) { }

  currentStep: Steps;
  allSteps: Steps[] = ["MAIN", "EDUCATION", "MEDICAL"];

  public createDoctorDTO: CreateDoctorDto;

  ngOnInit() {
    this.currentStep = "MAIN";
    this.createDoctorDTO = new CreateDoctorDto();

  }

  isStepMainValid: boolean = true;
  isStepMedicalValid: boolean = true;
  isStepEducationValid: boolean = true;
  isFormValid: boolean = false;

  public setInvalidStep(valid: boolean, step: Steps): void {
    this.checkValidAllForms();
    console.log(step + ' is ' + valid);

    if (step == "MAIN") {
      this.isStepMainValid = valid;
    }

    if (step == "EDUCATION") {
      this.isStepEducationValid = valid;
    }

    if (step == "MEDICAL") {
      this.isStepMedicalValid = valid;
    }
  }

  public getInvalidSteps(): Steps[] {
    let invalidSteps = [];

    if ((this.mainInformationComponent.mainForm != undefined) && (!this.mainInformationComponent.mainForm.valid)) {
      invalidSteps.push("MAIN");
      this.isStepMainValid = this.mainInformationComponent.mainForm.valid;
    }

    if ((this.educationInformationComponent.educationForm != undefined) && (!this.educationInformationComponent.educationForm.valid)) {
      invalidSteps.push("EDUCATION");
      this.isStepEducationValid = this.educationInformationComponent.educationForm.valid;
    }

    if ((this.medicalInformationComponent.medicalForm != undefined) && (!this.medicalInformationComponent.medicalForm.valid)) {
      invalidSteps.push("MEDICAL");
      this.isStepMedicalValid = this.medicalInformationComponent.medicalForm.valid;
    }

    invalidSteps.push("NONE");

    return invalidSteps;
  }

  public getFirstInvalidStep(): Steps {

    return this.getInvalidSteps()[0];
  }


  private saveAll(): void {

    this.createDoctorDTO.mainInformation = this.mainInformationComponent.mainForm.value;
    this.createDoctorDTO.educationInformation = this.educationInformationComponent.educationForm.value;
    this.createDoctorDTO.medicalInformation = this.medicalInformationComponent.medicalForm.value;
  }

  public save(): void {
    this.setInvalidSteps();
    let invalidStep = this.getFirstInvalidStep();
    if (invalidStep != "NONE") {
      this.setStep(invalidStep);
    }
  }

  public setStep(nextStep: Steps) {

    this.checkValidAllForms();
    this.saveAll();
    this.currentStep = nextStep;
  }


  public setNextStep(): void {
    let indexOfNextStep = this.allSteps.indexOf(this.currentStep) + 1;

    if (this.allSteps.length - 1 >= indexOfNextStep) {
      this.setStep(this.allSteps[indexOfNextStep]);
    }
  }

  public setPrevStep(): void {
    let indexOfPrevStep = this.allSteps.indexOf(this.currentStep) - 1;
    console.log(indexOfPrevStep);
    if (indexOfPrevStep >= 0) {
      this.setStep(this.allSteps[indexOfPrevStep]);
    }
  }

  public setInvalidSteps(): void {
    this.getInvalidSteps().forEach(step => {

      if (step == "MAIN") {
        this.isStepMainValid = this.mainInformationComponent.mainForm.valid && this.mainInformationComponent.mainForm.touched;
      }

      if (step == "EDUCATION") {
        this.isStepEducationValid = this.educationInformationComponent.educationForm.valid && this.educationInformationComponent.educationForm.touched;
      }

      if (step == "MEDICAL") {
        this.isStepMedicalValid = this.medicalInformationComponent.medicalForm.valid && this.medicalInformationComponent.medicalForm.touched;
      }

    });
  }

  public checkValidAllForms(): void {
    this.isFormValid = this.educationInformationComponent.educationForm.valid
      && this.mainInformationComponent.mainForm.valid
      && this.medicalInformationComponent.medicalForm.valid;
  }

  ngAfterViewInit() {
    this._cdr.detectChanges();
  }

}
