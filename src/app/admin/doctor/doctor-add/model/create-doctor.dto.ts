import { EducationInformationModel } from "./education-information.model";
import { MainInformationModel } from "./main-information.model";
import { MedicalInformationModel } from "./medical-information.model";

export class CreateDoctorDto {
  educationInformation: EducationInformationModel;
  mainInformation: MainInformationModel;
  medicalInformation: MedicalInformationModel;
}
