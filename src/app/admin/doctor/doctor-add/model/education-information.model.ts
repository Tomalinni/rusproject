export class EducationInformationModel {
  countryId: number;
  city: string;
  educationalInstitution: string;
  profession: string;
  endEducationDate: Date;
  diplomaNumber: string;
  diplomaScans: string[]
}
