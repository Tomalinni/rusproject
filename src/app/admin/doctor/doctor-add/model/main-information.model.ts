export class MainInformationModel {
  firstName: string;
  lastName: string;
  middleName: string;
  sex: string;
  birthday: Date;
  languages: number[];
  email: string;
  phone: string;
}
