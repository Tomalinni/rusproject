export class MedicalInformationModel {
  position: string;
  specializationIds: number[]
  categoryOfQualification: string;
  academicDegree: string;
  startYearWork: number;
}
