import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { DefaultComponent } from "../../theme/pages/default/default.component";
import { DoctorListComponent } from "./doctor-list/doctor-list.component";
import { DoctorAddComponent } from "./doctor-add/doctor-add.component";

const routes: Routes = [
  {
    path: '',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: DoctorListComponent
      },
      {
        path: 'add-doctor',
        component: DoctorAddComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }
