import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DoctorListComponent } from "./doctor-list/doctor-list.component";
import { EducationInformationComponent } from "./doctor-add/components/education-information.component";
import { MainInformationComponent } from "./doctor-add/components/main-information.component";
import { MedicalInformationComponent } from "./doctor-add/components/medical-information.component";
import { DoctorAddComponent } from "./doctor-add/doctor-add.component";
import { DoctorService } from "./doctor.service";
import { DoctorRoutingModule } from "./doctor-routing.module";
import { LayoutModule } from "../../theme/layouts/layout.module";
import {
  AutoCompleteModule, CalendarModule, DropdownModule, InputMaskModule, MultiSelectModule, OrderListModule,
  SelectButtonModule,
  SharedModule,
  ChipsModule
} from "primeng/primeng";
//import {ChipsModule} from 'primeng/chips';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TextMaskModule } from "angular2-text-mask";
import { SymbolsAndDashDirective } from "./doctor-add/directives/symbols-and-dash.directive";
import { DateDirective } from "./doctor-add/directives/date.directive";
import { FormUploadComponent } from "./doctor-add/components/upload/form-upload/form-upload.component";
import { UploadFileService } from "./doctor-add/components/upload/upload-file.service";
import { ListUploadComponent } from "./doctor-add/components/upload/list-upload/list-upload.component";
import { DetailsUploadComponent } from "./doctor-add/components/upload/details-upload/details-upload.component";




@NgModule({
  imports: [
    CommonModule,
    DoctorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    DropdownModule,
    OrderListModule,
    SelectButtonModule,
    InputMaskModule,
    CalendarModule,
    AutoCompleteModule,
    TextMaskModule,
    NgbModule,
    MultiSelectModule,
    ChipsModule
  ],
  declarations: [
    DoctorListComponent,
    EducationInformationComponent,
    MainInformationComponent,
    MedicalInformationComponent,
    DoctorAddComponent,
    SymbolsAndDashDirective,
    DateDirective,
    FormUploadComponent,
    DetailsUploadComponent,
    ListUploadComponent
  ],
  providers: [
    DoctorService,
    UploadFileService
  ]
})
export class DoctorModule { }
