import { Component, OnInit } from '@angular/core';
import { CreateFunction } from '../model/create-function.model';
import { FunctionsService } from '../services/functions.services'
import { FormBuilder, AbstractControl, FormControl, FormGroup, FormArray, Validators, ValidatorFn, AsyncValidatorFn } from "@angular/forms";
import { Localization } from '../model/localization.model';
import { SelectItem } from 'primeng/primeng';
import { Location } from '@angular/common';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { ModalConfirmComponent } from '../../modal/modal-confirm/modal-confirm.component';
import { ModalDefaultAttentionComponent } from '../../modal/modal-default-attention/modal-default-attention';
import { ModalsService } from "../../../_services/modals.service";



@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './function.component.html',
  styleUrls: ['./function.component.scss']
})
export class FunctionComponent implements OnInit {
  loading: boolean;
  maxLengthName: number = 200;
  maxLengthTestName: number = 100;
  title: string = 'Add function';
  author: string = 'user1';
  functionData: any;
  functionForm: FormGroup;
  id: number;
  authorId: number = 1;
  localizationList: SelectItem[];
  createDate: number;
  editDate: number = new Date().getTime();
  isEditTests: boolean[] = [];
  modelTest = [];
  nameFunction: string;

  constructor(
    private fb: FormBuilder,
    private functionsService: FunctionsService,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private modalsContentService: ModalsService,
  ) { }

  ngOnInit(): void {
    this.createForm();
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    // Надо как-то верно вклинить валидатор пока что так
    if (this.id)
      this.loadForm(this.id); // <-- тут находится еще валидатор с исключением
    else
      this.setUniqueNameValidate();
    this.loadLocalization();
  }

  createForm(): void {
    this.functionForm = this.fb.group({
      id: null,
      name: ['', [
        Validators.required,
        Validators.maxLength(this.maxLengthName),
        Validators.pattern(/^(?!.*\/).+$/)
      ]],
      localizations: this.fb.array([
        this.fb.group({ name: [null, Validators.required] })
      ], this.uniqueLocalizationsValidator()),
      tests: this.fb.array([], this.uniqueTestValidator())
    });
    this.functionForm.setValidators(this.functionNotTestValidator());
  }

  functionNotTestValidator(): ValidatorFn {
    return (formGroup: FormGroup): { [key: string]: any } => {
      let invalids = [];
      formGroup.get('tests').value.forEach((item, index) => {
        let errors = Object.assign({}, (formGroup.get('tests') as FormArray).controls[index].get('name').errors);
        if (typeof errors['notEqualToFunctionName'] !== 'undefined') delete errors['notEqualToFunctionName'];
        if (item.name === formGroup.get('name').value) errors['notEqualToFunctionName'] = true;
        if (Object.keys(errors).length === 0) errors = null;
        (formGroup.get('tests') as FormArray).controls[index].get('name').setErrors(errors)
      });
      return null;
    }
  }

  uniqueLocalizationsValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } => {
      let invalids = [];
      let arr = formArray.value
        .map((item, index) => {
          return { value: item.name, index: index }
        })
        .sort((a, b) => b.value - a.value);
      arr.forEach((item, index) => {
        if (typeof arr[index + 1] !== 'undefined' && item.value === arr[index + 1].value)
          invalids.push(arr[index + 1]);
      });
      formArray.controls.forEach(item => {
        let errors = Object.assign({}, item.get('name').errors);
        if (typeof errors['notUnique'] !== 'undefined') delete errors['notUnique'];
        if (Object.keys(errors).length === 0) errors = null;
        item.get('name').setErrors(errors);
      });
      invalids.forEach(item => {
        formArray.controls[item.index].get('name').setErrors({ 'notUnique': true })
      });
      return null;
    };
  }

  uniqueTestValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } => {
      let invalids = [];
      let temp = formArray.value;
      let arr = temp
        .map((item, index) => {
          return { value: item.name, index: index }
        })
        .sort((a, b) => {
          if (a.value > b.value) return 1;
          if (a.value < b.value) return -1;
          return 0;
        });
      arr.forEach((item, index) => {
        if (typeof arr[index + 1] !== 'undefined' && item.value === arr[index + 1].value)
          invalids.push(arr[index + 1]);
      });
      formArray.controls.forEach(item => {
        let errors = Object.assign({}, item.get('name').errors);
        if (typeof errors['notUnique'] !== 'undefined') delete errors['notUnique'];
        if (Object.keys(errors).length === 0) errors = null;
        item.get('name').setErrors(errors);
      });
      invalids.forEach(item => {
        formArray.controls[item.index].get('name').setErrors({ 'notUnique': true })
      });
      return null;
    };
  }

  noWhitespaceValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let isWhitespace = (control.value || '').trim().length === 0;
      let isValid = !isWhitespace;
      return isValid ? null : { 'required': true }
    };
  }

  setUniqueNameValidate(ignorable?: string): void {
    const callback = (isExist) => {
      if (isExist)
        this.functionForm.controls.name.setErrors({
          'notUnique': true
        });
    }
    this.functionForm.controls.name.valueChanges
      .debounceTime(700)
      .subscribe(val => {
        if (typeof ignorable !== 'undefined' && val === ignorable) return false;
        this.functionsService.existFunction(this.authorId, val).then(callback);
      });
  }

  loadLocalization() {
    this.functionsService.getLocalizations().then(
      (localizations: Localization[]) => {
        this.localizationList = localizations.map((l: Localization) => {
          return {
            label: l.name,
            value: l.id
          }
        });
      }
    );
  }

  loadForm(id: number) {
    this.title = 'Function';
    this.loading = true;
    this.functionsService.getFunction(id)
      .then(data => {
        this.author = data.author.name;
        this.authorId = data.author.id;
        this.createDate = data.createdAt;
        this.nameFunction = data.name;
        this.functionForm.patchValue({ name: data.name });
        this.setLocalizations(data.localizations);
        this.setTests(data.functionTests);
        this.isEditTests = data.functionTests.map(item => {
          this.modelTest.push(item.name);
          return false;
        });
        this.setUniqueNameValidate(data.name);
        this.loading = false;
      }, () => this.loading = false);
  }

  get localizations(): FormArray {
    return this.functionForm.get('localizations') as FormArray;
  }

  get tests(): FormArray {
    return this.functionForm.get('tests') as FormArray;
  }

  addLocalization() {
    this.localizations.push(this.fb.group({ name: [null, Validators.required] }));
  }

  addTest() {
    this.tests.push(this.fb.group({
      name: ['', [
        Validators.required,
        Validators.maxLength(this.maxLengthTestName),
        this.noWhitespaceValidator()
      ]]
    }));
    this.isEditTests.push(true);
  }

  setLocalizations(localizations: any[]) {
    const localizationsFG = localizations.map(l => this.fb.group({ name: l.id }));
    const localizationsFA = this.fb.array(localizationsFG, this.uniqueLocalizationsValidator());
    this.functionForm.setControl('localizations', localizationsFA);
  }

  setTests(tests: any[]) {
    const testsFG = tests.map(t => this.fb.group({
      name: [t.name, [
        Validators.required,
        Validators.maxLength(this.maxLengthTestName),
        this.noWhitespaceValidator()
      ]]    
}));
    const testsFA = this.fb.array(testsFG, this.uniqueTestValidator());
    this.functionForm.setControl('tests', testsFA);
  }

  removeLocalization(index: number) {
    this.isEditTests.splice(index, 1);
    this.localizations.removeAt(index);
  }

  editTest(index: number) {
    this.isEditTests[index] = true;
  }

  removeTest(index: number) {
    this.tests.removeAt(index);
    this.modelTest.splice(index, 1);
  }

  saveTest(index: number) {
    this.isEditTests[index] = false;
    this.modelTest[index] = this.tests.controls[index].get('name').value;
  }

  cancelTest(index: number) {
    if (this.isEditTests[index]) {
      if (typeof this.modelTest[index] === 'undefined') {
        this.removeTest(index);
      } else {
        this.tests.controls[index].setValue({ name: this.modelTest[index] });
      }
    }
  }

  save() {
    const localizationsId = this.localizations.value
      .filter(item => item.name !== '')
      .map(item => item.name);
    const tests = this.tests.value;
    const saveFunction: CreateFunction = {
      name: this.functionForm.get('name').value,
      localizations: localizationsId
    };
    if (tests.length > 0) saveFunction.tests = tests;
    console.log(saveFunction);

    if (this.id)
      this.functionsService.updateFunction(this.id, saveFunction)
        .then(() => {
          const modal = this.presentModalConfirm({
            name: this.modalsContentService.content.confirm.changeFunction.name,
            target: this.functionForm.get('name').value,
            event: this.modalsContentService.content.confirm.changeFunction.event
          });
          modal.result.then(() => this.back());
          setTimeout(() => modal.close(), 1000);
        })
    else
      this.functionsService.createFunction(saveFunction)
        .then(() => {
          const modal = this.presentModalConfirm({
            name: this.modalsContentService.content.confirm.addFunction.name,
            target: this.functionForm.get('name').value,
            event: this.modalsContentService.content.confirm.addFunction.event
          });
          modal.result.then(() => this.back());
          setTimeout(() => modal.close(), 1000);
        })
  }

  delete() {
    this.presentModalDelete(this.modalsContentService.content.attention.deleteFunction).result.then(() => {
      this.functionsService.removeFunction(this.id).then(() => {
        let modal = this.presentModalConfirm({
          name: this.modalsContentService.content.confirm.deleteFunction.name,
          target: this.nameFunction,
          event: this.modalsContentService.content.confirm.deleteFunction.event
        })
        modal.result.then(() => this.back());
        setTimeout(() => modal.close(), 1000);
      });
    });
  }

  back() {
    this.router.navigate(['admin/functions']);
  }

  presentModalDelete(content: { title: string, description: string, textOk: string, textCancel: string }): NgbModalRef {
    let modal = this.modalService.open(ModalDefaultAttentionComponent);
    modal.componentInstance.title = content.title;
    modal.componentInstance.description = content.description;
    modal.componentInstance.buttonTextOk = content.textOk;
    modal.componentInstance.buttonTextCancel = content.textCancel;
    return modal;
  }

  presentModalConfirm(content: { name: string, target: string, event: string }): NgbModalRef {
    let modal = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
    modal.componentInstance.nameConfirm = content.name;
    modal.componentInstance.targetConfirm = content.target;
    modal.componentInstance.eventConfirm = content.event;
    return modal;
  }

}
