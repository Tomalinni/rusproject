import { Component, OnInit, ViewChild, ElementRef, state } from '@angular/core';
import { FunctionModel } from '../model/function.model';
import { FunctionsService } from '../services/functions.services';
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { ModalDefaultAttentionComponent } from '../../modal/modal-default-attention/modal-default-attention';
import { ModalConfirmComponent } from '../../modal/modal-confirm/modal-confirm.component';
import { DataTable } from "primeng/components/datatable/datatable";
import { ModalsService } from "../../../_services/modals.service";

@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './functions-list.component.html',
  styles: []
})
export class FunctionsListComponent implements OnInit {
  //private dt: DataTable;
  @ViewChild('datatable') dt: DataTable;//set content(content: DataTable) { this.dt = content }

  functions: FunctionModel[] | any;

  dtIndexFrom: number = 1;
  dtIndexTo: number;
  dtLength: number = 0;
  dtFilterLength: number;
  switchControls: string;

  multiSortMeta = [
    { field: 'name', order: -1 }
  ];
  constructor(
    public functionsService: FunctionsService,
    public modalService: NgbModal,
    public modalsContentService: ModalsService
  ) { }

  ngOnInit(): void {
    this.functionsService.getFunctions().then(
      (functions: FunctionModel[]) => {
        this.functions = functions.map(fn => {
          return Object.assign({}, fn, { concatLocalization: fn.localizations.map(item => item.name).join(' ') });
        });
        this.updateCounter(functions.length);
      }
    );

  }

  deleteFunction(id: number, nameFunction: string) {
    this.presentModalDelete(this.modalsContentService.content.attention.deleteFunction).result.then(() => {
      this.functionsService.removeFunction(id).then(() => {
        this.removeRowDataTable(id);
        let modal = this.presentModalConfirm({
          name: this.modalsContentService.content.confirm.deleteFunction.name,
          target: nameFunction,
          event: this.modalsContentService.content.confirm.deleteFunction.event
        });
        setTimeout(() => modal.close(), 1000);
      });
    });
  }

  removeRowDataTable(id: number): void {
    let fns = JSON.parse(JSON.stringify(this.functions));
    const index = fns.findIndex(fn => fn.id === id);
    if (index !== -1) fns.splice(index, 1);
    this.functions = fns;
    this.updateCounter(fns.length);
  }

  presentModalDelete(content: { title: string, description: string, textOk: string, textCancel: string }): NgbModalRef {
    let modal = this.modalService.open(ModalDefaultAttentionComponent);
    modal.componentInstance.title = content.title;
    modal.componentInstance.description = content.description;
    modal.componentInstance.buttonTextOk = content.textOk;
    modal.componentInstance.buttonTextCancel = content.textCancel;
    return modal;
  }

  presentModalConfirm(content: { name: string, target: string, event: string }): NgbModalRef {
    let modal = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
    modal.componentInstance.nameConfirm = content.name;
    modal.componentInstance.targetConfirm = content.target;
    modal.componentInstance.eventConfirm = content.event;
    return modal;
  }

  rowTrackBy(index: number, row: any) { return row.id }

  updateCounter(length: number): void {
    length = (typeof this.dtFilterLength === 'undefined') ? length : this.dtFilterLength;
    let first = this.dt.first;
    let size = this.dt.rows;
    let diff = length - first;

    this.dtIndexTo = first + ((diff >= size) ? size : diff);
    this.dtIndexFrom = (first == 0) ? 1 : first + 1;
    this.dtLength = length;
  }

  countRows(event): void {
    this.updateCounter(this.functions.length);
  }

  countRowsFilter(event): void {
    this.dtFilterLength = event.filteredValue.length;
    this.updateCounter(event.filteredValue.length);
  }

  showContent(event): void {
    let el = event.target
      , elEllipsis = el.parentElement.parentElement
      , elHideControl = el.parentElement.children[1];

    elEllipsis.classList.remove('ellipsis_hidden-hide');
    elEllipsis.classList.add('ellipsis_hidden-show');
    el.setAttribute("style", "display: none;");
    elHideControl.setAttribute("style", "display: show;");
  }
  hideContent(event): void {
    let el = event.target
      , elEllipsis = el.parentElement.parentElement
      , elShowControl = el.parentElement.children[0];

    elEllipsis.classList.remove('ellipsis_hidden-show');
    elEllipsis.classList.add('ellipsis_hidden-hide');
    el.setAttribute("style", "display: none;");
    elShowControl.setAttribute("style", "display: show;");
  }
}
