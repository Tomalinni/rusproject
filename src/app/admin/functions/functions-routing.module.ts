import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultComponent } from "../../theme/pages/default/default.component";
import { FunctionsListComponent } from "./functions-list/functions-list.component";
import { FunctionComponent } from "./function/function.component";

const routes: Routes = [
  {
    path: '',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: FunctionsListComponent
      },
      {
        path: 'add-function',
        component: FunctionComponent
      },
      {
        path: 'edit-function/:id',
        component: FunctionComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FunctionsRoutingModule { }
