import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LayoutModule } from '../../theme/layouts/layout.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  SharedModule,
  DropdownModule,
  OrderListModule,
  SelectButtonModule,
  SlideMenuModule,
  SpinnerModule
} from 'primeng/primeng';
import { DataTableModule } from "primeng/components/datatable/datatable";
import { TextMaskModule } from 'angular2-text-mask';
import { FunctionsRoutingModule } from './functions-routing.module';

import { FunctionComponent } from './function/function.component';
import { FunctionsListComponent } from './functions-list/functions-list.component';

import { FunctionsService } from './services/functions.services';

@NgModule({
  imports: [
    CommonModule,
    FunctionsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    DropdownModule,
    OrderListModule,
    SelectButtonModule,
    SlideMenuModule,
    SpinnerModule,
    DataTableModule,
    TextMaskModule,
    NgbModule
  ],
  declarations: [
    FunctionsListComponent,
    FunctionComponent
  ],
  providers: [
    FunctionsService
  ]
})
export class FunctionsModule { }
