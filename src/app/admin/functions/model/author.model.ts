export class Author {
  id: number;
  name: string;
  avatarURL: string;
}
