import { Test } from './test.model';

export class CreateFunction {
  name: string;
  localizations: number[];
  tests?: Test[];
}

