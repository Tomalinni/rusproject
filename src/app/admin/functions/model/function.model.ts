import { Localization } from './localization.model';
import { Author } from './author.model';
import { Test } from './test.model';

export class FunctionModel {
  id: number;
  name: string;
  author: Author;
  //createAt: number;
  localizations: Localization[];
  createdAt: number;
  functionTests?: Test[];
  //localization: string[];
}

