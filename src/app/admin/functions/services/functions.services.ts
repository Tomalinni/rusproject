import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { FunctionModel } from "../model/function.model";
import { CreateFunction } from "../model/create-function.model";
import { Localization } from "../model/localization.model";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/debounceTime';

const currentUser = 'user1';

/*
* Сервис для работы со справочником функций
*/
@Injectable()
export class FunctionsService {

  constructor(private http: HttpClient) { }

  getFunctions(): Promise<FunctionModel[]> {
    const headers = this.getHeaders()
    const options = { headers: headers };

    return this.http
      .get(environment.baseUrl + '/rms-rehabilitation/api/v2/function', options)
      .map((response: any) => {
        response = response.map(item => {
          item.createdAt = item.createdAt - 1 * 3600 * 1000;
          return item;
        });
        return response as FunctionModel[]
      })
      .toPromise();
  }

  getFunction(id: number): Promise<FunctionModel> {
    const headers = this.getHeaders()
    const options = { headers: headers };

    return this.http
      .get(environment.baseUrl + `/rms-rehabilitation/api/v2/function/${id}`, options)
      .map((response: any) => {
        response.createdAt = response.createdAt - 1 * 3600 * 1000;
        return response as FunctionModel
      })
      .toPromise();
  }

  existFunction(authorId, nameFn): Promise<boolean> {
    const headers = this.getHeaders()
    const params = new HttpParams()
      .set('functionName', String(nameFn))
      .set('authorID', String(authorId));
    const options = { headers: headers, params: params };

    return this.http
      .get(environment.baseUrl + '/rms-rehabilitation/api/v2/function/exist', options)
      .map((response: { isExist: boolean }) => response.isExist)
      .toPromise();
  }

  getLocalizations(): Promise<Localization[]> {
    const headers = this.getHeaders()
    const options = { headers: headers };

    return this.http
      .get(environment.baseUrl + '/rms-rehabilitation/api/v2/bodypart', options)
      .map(response => response as Localization[])
      .toPromise();
  }

  createFunction(createFunction: CreateFunction): Promise<any> {
    const headers = this.getHeaders()
    const options = { headers: headers };

    return this.http
      .post(environment.baseUrl + '/rms-rehabilitation/api/v2/function', createFunction, options)
      .toPromise();
  }

  updateFunction(id: number, functionDTO: CreateFunction): Promise<any> {
    const headers = this.getHeaders();
    const options = { headers: headers, responseType: 'text' as 'json' };

    return this.http
      .put(environment.baseUrl + `/rms-rehabilitation/api/v2/function/${id}`, functionDTO, options)
      .toPromise();
  }

  removeFunction(id: number): Promise<any> {
    const headers = this.getHeaders();
    const options = { headers: headers };

    return this.http
      .delete(environment.baseUrl + `/rms-rehabilitation/api/v2/function/${id}`, options)
      .toPromise();
  }

  private getHeaders(): HttpHeaders {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' }).append('Authorization', 'Bearer' + token);
    return headers;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
