import { Component, Input, OnInit } from '@angular/core';

import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

import { ModalConfirmComponent } from './../modal-confirm/modal-confirm.component';

import { ModalsService } from "../../../_services/modals.service";

@Component({
  selector: 'modal-attention',
  templateUrl: './modal-attention.component.html',
  styles: []
})
export class ModalAttentionComponent implements OnInit {
  @Input() planName;
  @Input() textAttention;
  @Input() btnAttention;

  private modalRef;

  constructor(
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private modalsContentService: ModalsService
  ) { }

  ngOnInit() {
  }

  confirmClose() {
    this.activeModal.close();
    this.modalRef = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
    this.modalRef.componentInstance.nameConfirm = this.modalsContentService.contentConfirmPlan.name;
    this.modalRef.componentInstance.eventConfirm = this.modalsContentService.contentConfirmPlan.event;
    this.modalRef.componentInstance.targetConfirm = this.planName;
  }
}
