import { Component, OnInit } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'modal-add-new-admin',
  templateUrl: './modal-confirm-admin.component.html',
  styles: []
})
export class AddNewAdminModel implements OnInit {
  blockedUser = localStorage.getItem('fio')
  constructor(
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  }

}
