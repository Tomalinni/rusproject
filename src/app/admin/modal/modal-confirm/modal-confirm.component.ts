import { Component, Input, OnInit, AfterViewChecked, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styles: []
})
export class ModalConfirmComponent implements OnInit, AfterViewChecked {
  @Input() nameConfirm;
  @Input() eventConfirm;
  @Input() targetConfirm;
  @ViewChild('ellipsisText') ellipsisText: ElementRef;
  heightEllipsis: number;
  //blockedUser = localStorage.getItem('fio');

  constructor(
    private modalService: NgbModal,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit() {

  }

  ngAfterViewChecked() {
    this.heightEllipsis = this.getHeightEllipsis();
    this.cdRef.detectChanges();
  }
  getHeightEllipsis() {
    let h = this.ellipsisText.nativeElement.clientHeight;
    return h < 88 ? h : 88
  }
}
