import { Component, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'modal-default-attention',
  templateUrl: './modal-default-attention.component.html',
  styles: []
})
export class ModalDefaultAttentionComponent {
  @Input() title: string;
  @Input() description: string;
  @Input() buttonTextOk: string;
  @Input() buttonTextCancel: string;

  constructor(public activeModal: NgbActiveModal) { }

  onClickOk() { this.activeModal.close() }

  onClickCancel() { this.activeModal.dismiss() }
}
