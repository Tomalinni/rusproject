import { Component, Input, OnInit } from '@angular/core';

import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styles: []
})
export class ModalDialogComponent implements OnInit {
  @Input() planName;
  @Input() textAttention;
  @Input() btnAttention;
  @Input() btnCancel = "Отменить";


  constructor(
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {
  }

  confirmAttention() {
    this.activeModal.close(true);
  }

  confirmCancel() {
    this.activeModal.close(false);
  }
}
