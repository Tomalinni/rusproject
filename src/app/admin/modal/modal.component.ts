import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import { SelectItem } from 'primeng/primeng';
import { UserService } from "../../auth/_services/user.service";
import { ModalsService } from "../../_services/modals.service";

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styles: []
})
export class ModalComponent implements OnInit {

  private blockForm: FormGroup;
  private modalRef;
  public some;
  reasons: SelectItem[];
  reason: string = '';
  description: string = '';
  ban: boolean = false;

  requiredStatus: boolean;
  isDropdownValid: boolean;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private modalsContentService: ModalsService
  ) {
    this.requiredStatus = false;
    this.isDropdownValid = true;

    this.blockForm = this.formBuilder.group({
      reason: ['', Validators.compose([Validators.required])],
    });

    this.reasons = [];
  }

  userId;
  lockReasonTypeId;
  lockReasonComment = this.description;
  isAbleToBeRecovered;

  parse(data) {
    for (var i = 0; i < data.length; i++) {
      var psh = { 'label': data[i].name, 'value': data[i].id }
      this.reasons.push(psh);
      if (data.length == i) { break }
    }
  };

  ngOnInit() {

    this.userService.getBlockList().then(
      result => (this.some = result, console.log('result: ', result), this.parse(this.some)),
      err => (alert(err)));

  }

  confirmClose() {

    // if (this.userService.blockUser.status_code === 'ACTIVE') {
    //   this.userService.blockUser.status_code = 'LOCKED'
    // } else if (this.userService.blockUser.status_code === 'INACTIVE') {
    //   this.userService.blockUser.status_code = 'LOCKED_INACTIVE'
    // }

    this.userService.setBlockStatus(localStorage.getItem('id'));

    this.activeModal.close();
    this.modalRef = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
    this.modalRef.componentInstance.nameConfirm = this.modalsContentService.contentConfirmBlockUser.name;
    this.modalRef.componentInstance.eventConfirm = this.modalsContentService.contentConfirmBlockUser.event;
    this.modalRef.componentInstance.targetConfirm = localStorage.getItem('fio');
  }

  submitForm() {
    this.userId = localStorage.getItem('id');
    this.lockReasonTypeId = this.reason;
    this.isAbleToBeRecovered = !this.ban;
    this.userService.blockAdministrator(this.userId, this.lockReasonTypeId, this.lockReasonComment, this.isAbleToBeRecovered).then(
      result => (this.confirmClose()),
      err => (alert(err)));
  }

  getRequired(isRequired: boolean) {
    this.requiredStatus = isRequired;
  }
  dropdownValid(el) {
    let classOpenEl = el.container.className.indexOf('ui-dropdown-open');
    let isValue = el.value;

    if (classOpenEl > 0 && !isValue) {
      this.isDropdownValid = true;
    } else if (classOpenEl < 0 && !isValue) {
      this.isDropdownValid = false;
    }
  }
  onBlurDropdownValid(el) {
    let classInvalid = el.el.nativeElement.className.indexOf('ng-invalid');
    let classTouched = el.el.nativeElement.className.indexOf('ng-touched');
    let isValue = el.value;

    if (classInvalid > 0 && classTouched < 0 && !isValue) {
      this.isDropdownValid = true;
    }
  }
}
