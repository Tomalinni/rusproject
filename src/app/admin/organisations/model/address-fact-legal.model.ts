import { Address } from "./address.model";

export class AddressFactLegalModel {
  addressFact: Address;
  addressLegal: Address;
  isFactSameLegal: boolean;
  country: { label: string, value: string }
}
