export class Address {
  region: string;
  city: string;
  innerRegion: string;
  street: string;
  house: string;
  housing: string;
  index: string;
}
