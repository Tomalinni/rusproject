export class ContactInformation {
  countryPublicPhone: any;
  countrySpecialistPhone: any;
  countryHeadPhone: any;
  technicalSpecialist: string;
  technicalSpecialistPhone: string;
  technicalSpecialistEmail: string;
  head: string;
  headPhone: string;
  siteUrl: string
}
