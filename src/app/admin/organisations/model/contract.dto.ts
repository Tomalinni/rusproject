export class ContractDto {
  contractNumber: string;
  contractDateFrom: string;
  contractDateTo: string;
}
