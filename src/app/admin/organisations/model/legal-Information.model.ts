export class LegalInformation {
  inn: string;
  kpp: string;
  ogrn: string;
  contractNumber: string;
  contractDateFrom: Date;
  contractDateTo: Date;
}
