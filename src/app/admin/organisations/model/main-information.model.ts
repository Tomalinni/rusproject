export class MainInformationModel {
  shortName: string;
  fullName: string;
  organizationActionsDetail: string;
}
