export class OrganizationListDTO {
  id: number;
  name: string;
  address: string;
  statusName: string;
  statusCode: string;
  registrationDate: Date;
}
