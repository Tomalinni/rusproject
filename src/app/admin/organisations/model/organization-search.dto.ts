import { Address } from "./address.model";
import { ContactInformation } from "./contact-information.model";
import { LegalInformation } from "./legal-Information.model";
import { MainInformationModel } from "./main-information.model";
import { AddressFactLegalModel } from "./address-fact-legal.model";
import { OrganizationModel } from "./organization.model";
import { ContractDto } from "./contract.dto";

export class OrganizationSearchDto {
  registrationDateFrom: string;
  registrationDateTo: string;
  fullOrShortName: string;
  status: string;
}
