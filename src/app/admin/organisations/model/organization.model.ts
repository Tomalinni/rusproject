import { Address } from "./address.model";
import { ContactInformation } from "./contact-information.model";
import { LegalInformation } from "./legal-Information.model";
import { MainInformationModel } from "./main-information.model";
import { AddressFactLegalModel } from "./address-fact-legal.model";

export class OrganizationModel {
  id: number;
  mainInformation: MainInformationModel;
  address: AddressFactLegalModel;
  contactInformation: ContactInformation;
  legalInformation: LegalInformation;
}
