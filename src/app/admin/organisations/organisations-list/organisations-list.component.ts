import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationsService } from "../organizations.service";
import { OrganizationListDTO } from "../model/organization-list.dto";
import { SelectItem } from "primeng/primeng";
import { PrimengService } from "../../../_services/primeng.service";

@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './organisations-list.component.html',
  styles: []
})
export class OrganisationsListComponent implements OnInit {
  @ViewChild('datatable') dt;

  organizations: OrganizationListDTO[];
  rowFrom: number = 1;
  rowUntil: number;
  dtLength: number;
  filteredLength: number;
  isFiltered: string;
  datatable: any;
  dtRows: number;
  rangeDates: Date[];
  conditions: SelectItem[];
  condition: string;
  fullOrShortName: string;
  filteredOrgs: string[];
  ru: any;

  constructor(private organizationsService: OrganizationsService, private primengService: PrimengService, ) { }

  ngOnInit() {
    this.conditions = [
      { label: 'не выбрано', value: 'ALL' },
      { label: 'активная', value: 'ACTIVE' },
      { label: 'заблокирована', value: 'LOCKED_INACTIVE' },
      { label: 'не активна', value: 'NOT_ACTIVE' }
    ];

    this.ru = this.primengService.ru;
    this.startSearch();
  }
  countRows(event) {
    this.rowFrom = (event.first == 0) ? 1 : event.first + 1;
    this.dtLength = this.datatable.value.length;
    let _rowUntil = this.rowFrom + event.rows - 1;

    if (_rowUntil > this.dtLength) {
      return this.rowUntil = this.dtLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  countRowsFilter(event) {
    this.rowFrom = 1;
    let _rowUntil = this.rowFrom + this.datatable.rows - 1;
    this.dtLength = this.datatable.value.length;
    this.rowUntil = event.filteredValue.length;
    this.filteredLength = event.filteredValue.length;

    if (this.filteredLength == 0) {
      this.isFiltered = 'empty';
    } else if (this.filteredLength == 1) {
      this.isFiltered = 'one';
    } else if (this.dtLength == this.filteredLength) {
      this.isFiltered = '';
    } else {
      this.isFiltered = 'fromTo';
    }

    if (_rowUntil > this.filteredLength) {
      return this.rowUntil = this.filteredLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  getDt(el) {
    return this.datatable = el;
  }

  public startSearch(): void {

    console.log('start search');

    let dateFrom = this.rangeDates != undefined ? this.rangeDates[0] : undefined;
    let dateTo = this.rangeDates != undefined ? this.rangeDates[1] : undefined;

    if (dateTo == undefined)
      dateTo = dateFrom;

    this.reloadOrganizationsByDates(dateFrom, dateTo, this.fullOrShortName, this.condition);
  }

  private reloadOrganizationsByDates(dateFrom: Date, dateTo: Date, fullOrShortName: string, status: string) {
    console.log(dateFrom + '-' + dateTo + '-' + fullOrShortName + '-' + status);
    this.organizationsService.getOrganizationsByRegistrationDatesAndName(dateFrom, dateTo, fullOrShortName, status)
      .then(result => {
        this.organizations = result;
        this.calcPages();
        this.organizations.sort((a, b) => b.id - a.id)
      });
  }


  private calcPages() {
    this.dtLength = this.organizations.length;
    this.dtRows = this.dt.rows;

    if (this.dtLength <= this.dtRows) {
      this.rowUntil = this.dtLength;
    } else {
      this.rowUntil = this.dtRows
    }
  }

  filterOrgs(event) {
    this.filteredOrgs = [];

    for (let i = 0; i < this.organizations.length; i++) {
      let org = this.organizations[i].name;
      if (org.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filteredOrgs.push(org);
      }
    }
  }
}
