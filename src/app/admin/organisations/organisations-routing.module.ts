import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultComponent } from "../../theme/pages/default/default.component";
import { OrganisationsListComponent } from "./organisations-list/organisations-list.component";
import { OrganisationAddComponent } from "./organization-add/organisation-add.component";
import { AddAdminOrgComponent } from "../users/user/add-admin-org/add-admin-org.component";

const routes: Routes = [
  {
    path: '',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: OrganisationsListComponent
      },
      {
        path: 'add-organization',
        component: OrganisationAddComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganisationsRoutingModule { }
