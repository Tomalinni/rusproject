import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LayoutModule } from '../../theme/layouts/layout.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule, DropdownModule, OrderListModule, SelectButtonModule, SlideMenuModule, SpinnerModule, InputMaskModule, CalendarModule, AutoCompleteModule } from 'primeng/primeng';
import { DataTableModule } from "primeng/components/datatable/datatable";
import { TextMaskModule } from 'angular2-text-mask';

import { OrganisationsRoutingModule } from './organisations-routing.module';
import { OrganisationsListComponent } from './organisations-list/organisations-list.component';
import { OrganizationsService } from "./organizations.service";
import { OrganisationAddComponent } from "./organization-add/organisation-add.component";
import { MainInformationComponent } from "./organization-add/components/main-information.component";
import { AddressComponent } from "./organization-add/components/address.component";
import { ContactInformationComponent } from "./organization-add/components/contact-information.component";
import { LegalInformationComponent } from "./organization-add/components/legal-information.component";
import { AddAdminOrgComponent } from '../users/user/add-admin-org/add-admin-org.component';
import { CounterSymbolsModule } from "../../_components/counter-symbols/counter-symbols.module";
import { CounterRowsModule } from "../../_components/counter-rows/counter-rows.module";

@NgModule({
  imports: [
    CommonModule,
    OrganisationsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    DropdownModule,
    OrderListModule,
    SelectButtonModule,
    SlideMenuModule,
    SpinnerModule,
    InputMaskModule,
    CalendarModule,
    AutoCompleteModule,
    DataTableModule,
    TextMaskModule,
    NgbModule,
    CounterSymbolsModule,
    CounterRowsModule
  ],
  declarations: [
    OrganisationsListComponent, OrganisationAddComponent, MainInformationComponent, AddressComponent, ContactInformationComponent, LegalInformationComponent
  ],
  providers: [
    OrganizationsService, DatePipe
  ]
})

export class OrganisationsModule {

}
