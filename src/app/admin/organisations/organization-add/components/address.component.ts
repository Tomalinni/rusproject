import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Address } from "../../model/address.model";
import { Steps } from "../organisation-add.component";
import { OrganizationModel } from "../../model/organization.model";
import { AddressFactLegalModel } from "../../model/address-fact-legal.model";
import { PrimengService } from "../../../../_services/primeng.service";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";

@Component({
  selector: 'address-information',
  templateUrl: './address.component.html',
})
export class AddressComponent implements OnInit {

  @Input() organization: OrganizationModel;

  @Output() addressInformation = new EventEmitter<AddressFactLegalModel>();
  @Output() setStep = new EventEmitter<Steps>();
  @Output() cancelStep = new EventEmitter();
  @Output() onOverStep: EventEmitter<boolean> = new EventEmitter();

  public address: FormGroup;
  countryList: any;
  classDD: string;
  regions: string[];

  constructor(private primengService: PrimengService,
    private _script: ScriptLoaderService) {
    this.regions = primengService.regions;
  }

  ngOnInit() {
    this.address = new FormGroup({

      addressFact: new FormGroup({
        region: new FormControl('')
        , city: new FormControl('', [Validators.required, this.spaceValidation])
        , innerRegion: new FormControl('')
        , street: new FormControl('')
        , house: new FormControl('')
        , housing: new FormControl('')
        , building: new FormControl('')
        , office: new FormControl('')
        , index: new FormControl('', [Validators.required, this.spaceValidation])
        , isFactSameLegal: new FormControl('')
      })

      , addressLegal: new FormGroup({
        region: new FormControl('')
        , city: new FormControl('', [Validators.required, this.spaceValidationLegal])
        , innerRegion: new FormControl('')
        , street: new FormControl('')
        , house: new FormControl('')
        , housing: new FormControl('')
        , building: new FormControl('')
        , office: new FormControl('')
        , index: new FormControl('', [Validators.required, this.spaceValidationLegal])
      })

      , country: new FormControl({ label: 'Россия', value: 'RF' })
    });

    if (this.organization != undefined && this.organization.address != undefined) {
      this.address.patchValue(this.organization.address);
    }

    this.countryList = [
      { label: 'Россия', value: 'RF' },
      { label: 'Германия', value: 'DE' },
      { label: 'Франция', value: 'FR' },
      { label: 'США', value: 'US' }
    ]

    this.classDD = this.address.get('country').value.value;
  }

  public spaceValidation(fControl: FormControl) {
    let isSpace = (fControl.value || '').trim().length === 0;
    let isValid = !isSpace;
    return isValid ? null : { 'spaces': true }
  }

  public spaceValidationLegal(fControl: FormControl) {
    if (fControl.value !== 1) {
      let isSpace = (fControl.value || '').trim().length === 0;
      let isValid = !isSpace;
      return isValid ? null : { 'spaces': true }
    }
  }

  public setPrevStep({ value, valid }: { value: AddressFactLegalModel, valid: boolean }): void {
    this.addressInformation.emit(value);
    this.setStep.emit("MAIN");
  }

  nextStep({ value, valid }: { value: AddressFactLegalModel, valid: boolean }) {
    if (!this.address.get("addressFact.isFactSameLegal").value) {
      this.address.patchValue({
        addressLegal: this.address.get("addressFact").value
      });
    }
    this.addressInformation.emit(value);
    this.setStep.emit("CONTACT");
  }

  public cancel(): void {
    this.cancelStep.emit();
  }

  getModelDD(el) {
    this.classDD = el;
  }
  setOverStep() {
    return this.onOverStep.emit(true);
  }
  revertOverStep() {
    return this.onOverStep.emit(false);
  }

  filteredRegions: any[];

  filterRegions(event) {
    this.filteredRegions = [];
    if (this.classDD === 'RF') {
      for (let i = 0; i < this.regions.length; i++) {
        let region = this.regions[i];
        if (region.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
          this.filteredRegions.push(region);
        }
      }
    }
  }

  factIsSameLegal() {

    if (!this.address.get("addressFact.isFactSameLegal").value) {
      this.address.patchValue({
        addressLegal: {
          city: 1,
          index: 1
        }
      });
    }
    else {
      this.address.get("addressLegal").reset();
    }
  }

  ngAfterViewInit() {
    this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
      'assets/app/js/components/organizations/organization-add/address.js');

  }
}
