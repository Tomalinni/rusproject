import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { OrganizationModel } from "../../model/organization.model";
import { DragulaService } from "ng2-dragula";
import { ContactInformation } from "../../model/contact-information.model";
import { Steps } from "../organisation-add.component";
import { AddressFactLegalModel } from "../../model/address-fact-legal.model";
import { PrimengService } from "../../../../_services/primeng.service";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";

@Component({
  selector: 'contact-information',
  templateUrl: './contact-information.component.html',
})
export class ContactInformationComponent implements OnInit {

  @Input() organization: OrganizationModel;
  @Output() contactInformation = new EventEmitter<ContactInformation>();
  @Output() setStep = new EventEmitter<Steps>();
  @Output() cancelStep = new EventEmitter();
  @Output() onOverStep: EventEmitter<boolean> = new EventEmitter();

  public contact: FormGroup;
  countryList: any;
  maskList: any;
  country: string;
  countryPublic: string;
  countrySpecialist: string;
  countryHead: string;
  maskPublic: string;
  placeholderMaskPublic: string;
  maskSpecialist: string;
  placeholderMaskSpecialist: string;
  maskHead: string;
  placeholderMaskHead: string;
  mdlCountryPublicPhone: string;
  mdlCountrySpecialistPhone: string;
  mdlCountryHeadPhone: string;

  constructor(
    private primengService: PrimengService,
    private _script: ScriptLoaderService
  ) {
    this.countryList = primengService.countryList;
    this.maskList = primengService.maskList;
  }

  ngOnInit() {
    this.contact = new FormGroup({
      publicEmail: new FormControl('', Validators.pattern("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+"))
      , publicPhone: new FormControl('')
      , countryPublicPhone: new FormControl('')
      , siteUrl: new FormControl('')
      , technicalSpecialist: new FormControl('', [Validators.required, Validators.pattern(/^([a-zA-Z]*[a-zA-Z\s]*)$|^([а-яА-ЯёЁ]*[а-яА-ЯёЁ\s]*)$/)])
      , technicalSpecialistPhone: new FormControl('', [Validators.required, this.spaceValidation])
      , countrySpecialistPhone: new FormControl('')
      , technicalSpecialistEmail: new FormControl('', Validators.pattern("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+"))
      , head: new FormControl('', Validators.pattern(/^([a-zA-Z]*[a-zA-Z\s]*)$|^([а-яА-ЯёЁ]*[а-яА-ЯёЁ\s]*)$/))
      , headPhone: new FormControl('')
      , countryHeadPhone: new FormControl('')
    });

    if (this.organization != undefined && this.organization.contactInformation != undefined) {
      this.contact.patchValue(this.organization.contactInformation);

    }

    this.country = this.organization.address.country.value;

    if (!this.contact.get('publicPhone').value) {
      this.countryPublic = this.country;
      this.mdlCountryPublicPhone = this.country;
    } else {
      let _country = this.organization.contactInformation.countryPublicPhone;

      this.countryPublic = _country;
      this.mdlCountryPublicPhone = _country;
    }

    if (!this.contact.get('technicalSpecialistPhone').value) {
      this.countrySpecialist = this.country;
      this.mdlCountrySpecialistPhone = this.country;
    } else {

      let _country = this.organization.contactInformation.countrySpecialistPhone;

      this.countrySpecialist = _country;
      this.mdlCountrySpecialistPhone = _country;
    }

    if (!this.contact.get('headPhone').value) {
      this.countryHead = this.country;
      this.mdlCountryHeadPhone = this.country;
    } else {
      let _country = this.organization.contactInformation.countryHeadPhone;
      this.countryHead = _country;
      this.mdlCountryHeadPhone = _country;
    }

    console.log("Current country is: " + this.country);

    this.maskPublic = this.maskList[this.countryPublic].mask;
    this.placeholderMaskPublic = this.maskList[this.countryPublic].placeholder;
    this.maskSpecialist = this.maskList[this.countrySpecialist].mask;
    this.placeholderMaskSpecialist = this.maskList[this.countrySpecialist].placeholder;
    this.maskHead = this.maskList[this.countryHead].mask;
    this.placeholderMaskHead = this.maskList[this.countryHead].placeholder;

  }
  public spaceValidation(fControl: FormControl) {
    let isSpace = (fControl.value || '').trim().length === 0;
    let isValid = !isSpace;
    return isValid ? null : { 'spaces': true }
  }

  public setPrevStep({ value, valid }: { value: ContactInformation, valid: boolean }): void {
    this.contactInformation.emit(value);
    this.setStep.emit("ADDRESS")
  }

  public nextStep({ value, valid }: { value: ContactInformation, valid: boolean }) {
    this.contactInformation.emit(value);
    this.setStep.emit("LEGAL")
  }

  public cancel(): void {
    this.cancelStep.emit();
  }
  getFieldCountry(fld) {
    let control = fld.name;

    if (control == 'countryPublicPhone') {
      this.countryPublic = fld.value;
      this.maskPublic = this.maskList[this.countryPublic].mask;
      this.placeholderMaskPublic = this.maskList[this.countryPublic].placeholder;
    } else if (control == 'countrySpecialistPhone') {
      this.countrySpecialist = fld.value;
      this.maskSpecialist = this.maskList[this.countrySpecialist].mask;
      this.placeholderMaskSpecialist = this.maskList[this.countrySpecialist].placeholder;
    } else if (control == 'countryHeadPhone') {
      this.countryHead = fld.value;
      this.maskHead = this.maskList[this.countryHead].mask;
      this.placeholderMaskHead = this.maskList[this.countryHead].placeholder;
    }
  }
  setOverStep() {
    return this.onOverStep.emit(true);
  }
  revertOverStep() {
    return this.onOverStep.emit(false);
  }

  ngAfterViewInit() {
    this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
      'assets/app/js/components/organizations/organization-add/contact.js');

  }

}
