import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { OrganizationModel } from "../../model/organization.model";
import { DragulaService } from "ng2-dragula";
import { ContactInformation } from "../../model/contact-information.model";
import { Steps } from "../organisation-add.component";
import { LegalInformation } from "../../model/legal-Information.model";

import { PrimengService } from "../../../../_services/primeng.service";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { CompileMetadataResolver } from "@angular/compiler";

@Component({
  selector: 'legal-information',
  templateUrl: './legal-information.component.html',
})
export class LegalInformationComponent implements OnInit {

  @Input() organization: OrganizationModel;
  @Output() legalInformation = new EventEmitter<LegalInformation>();
  @Output() setStep = new EventEmitter<Steps>();
  @Output() save = new EventEmitter();
  @Output() cancelStep = new EventEmitter();
  @Output() onOverStep: EventEmitter<boolean> = new EventEmitter();

  ru: any;
  public legal: FormGroup;

  dateFromMin: Date = new Date(1918, 0, 1);
  dateToMin: Date = new Date(2000, 0, 1);

  checkMinValueFrom() {
    if (this.dateFromMin > this.legal.get("contractDateFrom").value) {
      this.legal.get("contractDateFrom").setValue(this.dateFromMin);
    }

  }

  checkMinValueTo() {
    if (this.dateToMin > this.legal.get("contractDateTo").value) {
      this.legal.get("contractDateTo").setValue(this.dateToMin);
    }
  }

  constructor(private primengService: PrimengService, private _script: ScriptLoaderService) {
    this.ru = primengService.ru;
  }

  ngOnInit() {

    this.legal = new FormGroup({
      inn: new FormControl('', [Validators.required, innValid, this.spaceValidation])
      , kpp: new FormControl('')
      , ogrn: new FormControl('')
      , contractNumber: new FormControl('', [Validators.required, this.spaceValidation])
      , contractDateFrom: new FormControl('')
      , contractDateTo: new FormControl('')
    });

    if (this.organization != undefined && this.organization.legalInformation != undefined) {
      this.legal.patchValue(this.organization.legalInformation);
    }

  }

  public dateCheck() {
    if (this.legal.get("contractDateFrom").value === '' && this.legal.get("contractDateTo").value === '') {
      return true;
    } else {
      if (this.legal.get("contractDateTo").value !== '') {
        if (this.legal.get("contractDateFrom").value !== '' && this.legal.get("contractDateFrom").value < this.legal.get("contractDateTo").value) {
          return true;
        } else if (this.legal.get("contractDateFrom").value === '') {
          return false;
        } else {
          return false;
        }
      } else {
        return true;
      }
    }
  }

  public spaceValidation(fControl: FormControl) {
    let isSpace = (fControl.value || '').trim().length === 0;
    let isValid = !isSpace;
    return isValid ? null : { 'spaces': true }
  }

  public setPrevStep({ value, valid }: { value: LegalInformation, valid: boolean }): void {
    this.legalInformation.emit(value);
    this.setStep.emit("CONTACT")
  }

  public nextStep({ value, valid }: { value: LegalInformation, valid: boolean }) {

    if (this.dateCheck()) {
      this.legalInformation.emit(value);
      this.save.emit();
    }

  }

  public cancel(): void {
    this.cancelStep.emit();
  }

  setOverStep() {
    return this.onOverStep.emit(true);
  }

  revertOverStep() {
    return this.onOverStep.emit(false);
  }

  ngAfterViewInit() {
    this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
      'assets/app/js/components/organizations/organization-add/legal.js');
  }
}

function innValid(input: FormControl) {
  if (input.value != undefined) {
    const isInnLengthInvalid = input.value.toString().length != 10 && input.value.toString().length != 12;
    return isInnLengthInvalid ? { wrongInn: true } : null;
  }
  return null;
}
