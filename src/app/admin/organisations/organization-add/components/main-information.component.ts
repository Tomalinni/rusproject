import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { OrganizationModel } from "../../model/organization.model";
import { MainInformationModel } from "../../model/main-information.model";
import { Steps } from "../organisation-add.component";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";

@Component({
  selector: 'main-information',
  templateUrl: './main-information.component.html',
})
export class MainInformationComponent implements OnInit {

  @Input() organization: OrganizationModel;
  @Output() mainInformation = new EventEmitter<MainInformationModel>();
  @Output() setStep = new EventEmitter<Steps>();
  @Output() cancelStep = new EventEmitter();
  @Output() onOverStep: EventEmitter<boolean> = new EventEmitter();

  public clinicMainInformation: FormGroup;
  maxfullName: number = 200;
  maxOrgDetail: number = 500;

  constructor(private _script: ScriptLoaderService) { }

  ngOnInit() {
    this.clinicMainInformation = new FormGroup({
      shortName: new FormControl('', [
        Validators.required,
        Validators.maxLength(100),
        this.spaceValidation
      ])
      , fullName: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(this.maxfullName),
        this.spaceValidation
      ]))
      , organizationActionsDetail: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(this.maxOrgDetail),
        this.spaceValidation
      ]))
    });

    console.log(this.organization);

    if (this.organization != undefined && this.organization.mainInformation != undefined) {
      this.clinicMainInformation.patchValue(this.organization.mainInformation);
    }

  }

  public spaceValidation(fControl: FormControl) {
    let isSpace = (fControl.value || '').trim().length === 0;
    let isValid = !isSpace;
    return isValid ? null : { 'spaces': true }
  }

  nextStep({ value, valid }: { value: MainInformationModel, valid: boolean }) {
    this.mainInformation.emit(value)
    this.setStep.emit("ADDRESS");
  }

  public cancel(): void {
    this.cancelStep.emit();
  }
  setOverStep() {
    return this.onOverStep.emit(true);
  }

  ngAfterViewInit() {
    this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
      'assets/app/js/components/organizations/organization-add/main-informations.js');

  }

}
