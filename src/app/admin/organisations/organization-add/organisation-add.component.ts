import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationsService } from "../organizations.service";
import { FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { OrganizationModel } from "../model/organization.model";
import { Address } from "../model/address.model";
import { ContactInformation } from "../model/contact-information.model";
import { LegalInformation } from "../model/legal-Information.model";
import { AddressFactLegalModel } from "../model/address-fact-legal.model";
import { MainInformationModel } from "../model/main-information.model";
import { AddressComponent } from "./components/address.component";
import { MainInformationComponent } from "./components/main-information.component";
import { ContactInformationComponent } from "./components/contact-information.component";
import { LegalInformationComponent } from "./components/legal-information.component";
import { ModalAttentionComponent } from "../../modal/modal-attention/modal-attention.component";
import { ModalConfirmComponent } from "../../modal/modal-confirm/modal-confirm.component";
import { ModalsService } from "../../../_services/modals.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute, Router } from "@angular/router";
import { ModalDialogComponent } from "../../modal/modal-dialog/modal-dialog.component";
import { Observable } from "rxjs/Observable";
import { OrganizationDto } from "../model/organization.dto";
import { ContractDto } from "../model/contract.dto";
import { DatePipe } from "@angular/common";

export type Steps = "MAIN" | "ADDRESS" | "CONTACT" | "LEGAL";


@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './organisation-add.component.html',
})
export class OrganisationAddComponent implements OnInit {

  @ViewChild(ContactInformationComponent) contact: ContactInformationComponent;
  @ViewChild(AddressComponent) address: AddressComponent;
  @ViewChild(LegalInformationComponent) legal: LegalInformationComponent;
  @ViewChild(MainInformationComponent) main: MainInformationComponent;

  private modalRef;
  private timer;
  overStep1: boolean;
  overStep2: boolean;
  overStep3: boolean;
  overStep4: boolean;

  saveAll(): void {

    if (this.main != undefined) {
      this.setMainInformation(this.main.clinicMainInformation.value);
    }

    if (this.address != undefined) {
      this.setAddress(this.address.address.value);
    }

    if (this.legal != undefined)
      this.setLegalInformation(this.legal.legal.value);

    if (this.contact != undefined)
      this.setContactInformation(this.contact.contact.value);
  }

  isValidAll(): boolean {

    if (this.main != undefined)
      return (this.main.clinicMainInformation.valid);

    if (this.address != undefined) {
      return this.address.address.valid;
    }

    if (this.legal != undefined) {
      return this.legal.legal.valid;
    }
    if (this.contact != undefined)
      return this.contact.contact.valid;

  }

  model: OrganizationModel;
  currentStep: Steps;
  isValid: boolean;

  constructor(private organizationsService: OrganizationsService,
    private modalService: NgbModal,
    private modalsContentService: ModalsService,
    private route: Router,
    private datepipe: DatePipe
  ) { }

  ngOnInit() {
    this.model = new OrganizationModel();
    this.currentStep = "MAIN";
  }

  public setMainInformation(mainInfo: MainInformationModel): void {
    this.model.mainInformation = mainInfo;
  }

  public setStep(nextStep: Steps) {
    this.currentStep = nextStep;
    console.log(this.model);
  }

  public setAddress(address: AddressFactLegalModel): void {

    this.model.address = address;
  }

  public setContactInformation(contactInformation: ContactInformation): void {

    this.model.contactInformation = contactInformation;
  }

  public setLegalInformation(legalInformation: LegalInformation): void {

    this.model.legalInformation = legalInformation;
  }

  public save(): void {
    let dto = new OrganizationDto();
    dto.organization = this.model;



    dto.contract = new ContractDto();
    dto.contract.contractDateFrom = this.model.legalInformation.contractDateFrom != undefined ? this.datepipe.transform(this.model.legalInformation.contractDateFrom, 'yyyy-MM-dd') : undefined;
    dto.contract.contractDateTo = this.model.legalInformation.contractDateTo != undefined ? this.datepipe.transform(this.model.legalInformation.contractDateTo, 'yyyy-MM-dd') : undefined;
    dto.contract.contractNumber = this.model.legalInformation.contractNumber;

    this.organizationsService.saveOrganization(dto).then(result => {

      this.model.id = result;

      this.showAddAdminClinicDialog();

    }).catch(() => {
      this.showErrorModal()
    });
  }

  public showAddAdminClinicDialog() {
    this.modalRef = this.modalService.open(ModalDialogComponent, { size: 'sm' });
    this.modalRef.componentInstance.textAttention = this.modalsContentService.content.attention.addAdminOrg.text;
    this.modalRef.componentInstance.btnAttention = this.modalsContentService.content.attention.addAdminOrg.buttonSuccess;
    this.modalRef.componentInstance.btnCancel = this.modalsContentService.content.attention.addAdminOrg.buttonCancel;

    this.modalRef.result.then((res) => {
      if (res) {
        this.route.navigate(['/admin/users/add-admin', this.model.id])
      }
      else {
        this.showDoneCreatingModal();
      }
    });
  }

  public showDoneCreatingModal() {
    this.modalRef = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
    this.modalRef.componentInstance.nameConfirm = this.modalsContentService.content.confirm.addOrganization.name;
    this.modalRef.componentInstance.targetConfirm = this.model.mainInformation.fullName;
    this.modalRef.componentInstance.eventConfirm = this.modalsContentService.content.confirm.addOrganization.event;
    this.timer = Observable.timer(2000, 1000);
    this.timer.subscribe(t => {
      this.modalRef.close();
      this.route.navigate(['admin/organisations'])
      this.timer.stop();
    });
  }

  public showErrorModal() {
    this.modalRef = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
    this.modalRef.componentInstance.nameConfirm = this.modalsContentService.content.error.addOrganization.name;
    this.modalRef.componentInstance.targetConfirm = this.model.mainInformation.fullName;
    this.modalRef.componentInstance.eventConfirm = this.modalsContentService.content.error.addOrganization.event;
  }

  public cancelEdit(): void {
    this.modalRef = this.modalService.open(ModalDialogComponent);
    this.modalRef.componentInstance.textAttention = this.modalsContentService.content.attention.addOrganization.text;
    this.modalRef.componentInstance.btnAttention = this.modalsContentService.content.attention.addOrganization.button;
    this.modalRef.result.then((result) => {
      console.log(result);
      if (result) {
        this.route.navigate(['admin/organisations']);
      }
    });
  }
  getOverStep(atr) {
    if (this.currentStep == 'ADDRESS') {
      this.overStep1 = atr;
    } else if (this.currentStep == 'CONTACT') {
      this.overStep2 = atr;
    } else if (this.currentStep == 'LEGAL') {
      this.overStep3 = atr;
    }


  }

}
