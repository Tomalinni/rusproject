import { Injectable } from "@angular/core";
import { Headers, RequestOptions } from "@angular/http";
import { environment } from "../../../environments/environment";
import { OrganizationModel } from "./model/organization.model";
import { HttpClient } from "@angular/common/http";
import { OrganizationDto } from "./model/organization.dto";
import { OrganizationListDTO } from "./model/organization-list.dto";
import { OrganizationSearchDto } from "./model/organization-search.dto";
import { DatePipe } from "@angular/common";

/*
* Сервис для работы с организациями
*/
@Injectable()
export class OrganizationsService {

  private organizationAPI: string = environment.baseUrl + "/rms-personal/organization";

  constructor(private http: HttpClient, private datepipe: DatePipe) { }

  public getOrganization(id: number): Promise<OrganizationModel> {
    return this.http
      .get(this.organizationAPI + '/' + id)
      .toPromise()
      .then((response) => {
        return response as OrganizationModel;
      })
      .catch(this.handleError);
  }

  public getOrganizationsByRegistrationDatesAndName(registrationDateFrom: Date, registrationDateTo: Date, fullOrShortName: string, status: string): Promise<OrganizationListDTO[]> {
    let searchDto = new OrganizationSearchDto();
    searchDto.fullOrShortName = fullOrShortName;
    searchDto.registrationDateFrom = registrationDateFrom != undefined ? this.datepipe.transform(registrationDateFrom, 'yyyy-MM-dd') : undefined;
    searchDto.registrationDateTo = registrationDateTo != undefined ? this.datepipe.transform(registrationDateTo, 'yyyy-MM-dd') : undefined;
    searchDto.status = status == "ALL" ? undefined : status;

    console.log(searchDto)

    return this.http
      .post(this.organizationAPI + '/search',
      searchDto
      )
      .toPromise()
      .then((response) => {
        return response as OrganizationListDTO[];
      })
      .catch(this.handleError);
  }

  public saveOrganization(organizationDto: OrganizationDto): Promise<number> {
    return this.http
      .post(this.organizationAPI + "/withContract",
      organizationDto
      )
      .toPromise()
      .then((response) => {
        return response as number;
      });
  }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
