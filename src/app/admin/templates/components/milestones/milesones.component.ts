import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'milestones',
  templateUrl: './milestones.component.html',
  styleUrls: [
    './milestones.component.scss',
    '../../../../../assets/css/vendors/ng2-dragula/dragula.css'
  ]
})
export class MilestonesComponent implements OnInit {
  @Input() formTemplateMilestone: FormGroup;
  @Input() milestones;
  @Output() selectMilestone = new EventEmitter<any>();

  constructor(private dragulaService: DragulaService) { }

  select(phase) { this.selectMilestone.emit(phase) }

  ngOnInit() {
    let mils = this.milestones;
    this.dragulaService.setOptions("rehab", {
      direction: 'horizontal',
      invalid: function(el, handle) {
        return mils.length < 2;
      },
    });
  }

  ngOnDestroy() {
    this.dragulaService.destroy('rehab');
  }

}
