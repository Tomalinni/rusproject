import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';

import { TemplateService } from "./template.service";
import { Injure } from "./models/injure.model";

@Injectable()
export class InjuryResolveService implements Resolve<Injure[]> {

  constructor(private templateService: TemplateService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Promise<Injure[]> {
    return this.templateService.getInjury().then(inj => {
      if (inj) {
        return inj;
      } else {
        this.router.navigate(['']);
        return false;
      }
    });
  }

}
