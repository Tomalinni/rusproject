export class EditPlanArgumentsDTO {
  name: string;
  injury: number;
  phases: number[] = [];

}
