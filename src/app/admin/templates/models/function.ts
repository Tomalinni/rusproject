import { FunctionLookupValue } from "./functionLookupValue";

export class Function {
  id: number;
  localizedName: string;
  unitTypeId: number;
  unitTypeCode: String;
  unitId: number;
  localizedUnitName: string;
  values: FunctionLookupValue[] = [];
}
