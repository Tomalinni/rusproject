import { FunctionLookupValue } from "./functionLookupValue";

export class FunctionModel {

  function: number;
  lookupValue: number;
  valueBoolean: boolean;
  valueNumeric: number;
  valueNumericMax: number;
  localizedUnitName: string;
  localizedName: string;
  unitTypeCode: String;
  values: FunctionLookupValue[] = [];

}
