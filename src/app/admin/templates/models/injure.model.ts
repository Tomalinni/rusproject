export class Injure {
  constructor(public code: string, public id: number, public localizedName: string) { }
}
