import { FunctionModel } from "./functionModel";

export class PhaseDTO {
  id: number;
  name: string;
  weeks: number;
  days: number;
  goals: FunctionModel[] = [];
}
