import { PhaseDTO } from "./phaseDTO";

export class PlanDTO {
  id: number;
  name: string;
  injury: number;
  phases: PhaseDTO[] = [];
}
