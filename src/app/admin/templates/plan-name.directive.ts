import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { FormControl } from "@angular/forms";
import { TemplateService } from "./template.service";

@Directive({
  selector: '[validatePlanName]'
})

export class PlanNameDirective {

  @Input('validateFormControl') validateFormControl: FormControl;
  @Input('currentName') currentName: String;

  constructor(private service: TemplateService) { }

  @HostListener('focus', ['$event.target'])
  onFocus(target) {
    console.log("Focus called");

    this.validateFormControl.markAsUntouched();

    console.log(this.validateFormControl.touched);
  }

  @HostListener('focusout', ['$event.target'])
  onFocusout(target) {
    this.service.isPlanNameExists(this.validateFormControl.value).then(result => {
      console.log("Focus out called:" + this.currentName);

      if ((result) && (this.currentName != this.validateFormControl.value)) {
        this.validateFormControl.setErrors({
          "notUnique": result
        });
      }
      else {
        this.validateFormControl.setErrors(null);
      }
    });
    console.log("Focus out called");
  }
}
