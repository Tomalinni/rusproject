import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";

import { ModalAttentionComponent } from '../../modal/modal-attention/modal-attention.component';
import { ModalConfirmComponent } from '../../modal/modal-confirm/modal-confirm.component';

import { ModalsService } from "../../../_services/modals.service";

//import { DragulaService } from 'ng2-dragula';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { Router } from "@angular/router";
import { EditPlanArgumentsDTO } from "../models/editPlanArgumentsDTO";
import { FunctionModel } from "../models/functionModel";
import { PhaseDTO } from "../models/phaseDTO";
import { FunctionLookupValue } from "../models/functionLookupValue";
import { PlanDTO } from "../models/planDTO";
import { Function } from "../models/function";
import { TemplateService } from "../template.service";
import { ActivatedRoute } from '@angular/router';
import { number } from "ng2-validation/dist/number";
import { Injure } from "../models/injure.model";

const BOOLEAN: String = "BOOLEAN";
const INT: String = "INT";
const FLOAT: String = "FLOAT";
const ENUM: String = "ENUM";
const INT_RANGE: String = "INT_RANGE";
const FLOAT_RANGE: String = "FLOAT_RANGE";
const DAY: String = "Day";
const WEEK: String = "Week";


@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './rehabilitation.component.html',
  styleUrls: ['../../../../assets/css/vendors/ng2-dragula/dragula.css']
})
export class RehabilitationComponent implements OnInit {
  private modalRef;

  injuresList: Injure[];
  functionsList: Function[];

  currentPlan: PlanDTO = new PlanDTO();
  currentFunctions: FunctionModel[] = [];

  currentPhaseForEditId: number = null;
  currentUnitTypeCode: string;
  currentLocalizedUnitName: string;
  currentFunctionsValuesList: FunctionLookupValue[] = [];

  isFieldDirty: boolean = false;
  isMilestoneEdit: boolean;
  isShowAllMilestones: boolean;
  isMilestoneSave: boolean = true;

  phaseWeekDuration: number = 0;
  maxMilestoneName: number = 55;

  formTemplateHeader: FormGroup;
  formTemplateMilestone: FormGroup;

  milestoneFunctionValueFormControl: AbstractControl;
  milestoneFunctionValueMinFormControl: AbstractControl;
  milestoneFunctionValueMaxFormControl: AbstractControl;
  milestoneFunctionValueBooleanFormControl: AbstractControl;
  milestoneFunctionValueEnumFormControl: AbstractControl;
  milestoneNameFormControl: AbstractControl;
  planNameFormControl: AbstractControl;
  injuryFormControl: AbstractControl;


  mFunctionValueEnum: number;
  mFunctionValueEnumTarget: string;
  mFunctionValueMin: number;

  maskPercent = createNumberMask({
    prefix: '',
    suffix: '%',
    integerLimit: 3
  });
  maskMM = createNumberMask({
    prefix: '',
    suffix: ' °',
    allowDecimal: 'true',
    decimalLimit: 3
  });

  mstDurationOpt = [
    { label: "Day", value: 'Day' },
    { label: "Week", value: 'Week' },
  ];

  constructor(private templateService: TemplateService,
    private router: Router,
    private formBuilder: FormBuilder,
    //private dragulaService: DragulaService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private modalsContentService: ModalsService) {

    this.formTemplateHeader = this.formBuilder.group({
      injure: ['', Validators.compose([Validators.required])],
      planName: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
    });

    this.formTemplateMilestone = this.formBuilder.group({
      milestoneName: ['', Validators.compose([Validators.required, Validators.maxLength(this.maxMilestoneName)])],
      milestoneValueDuration: ['', Validators.compose([Validators.required])],
      milestoneDuration: ['', Validators.compose([Validators.required])],
      milestoneFunction: ['', Validators.compose([Validators.required])],
      milestoneFunctionValue: ['', Validators.compose([Validators.required, Validators.max(100)])],
      milestoneFunctionValueMin: ['', Validators.compose([Validators.required, Validators.max(100)])],
      milestoneFunctionValueMax: ['', Validators.compose([Validators.required, Validators.max(100)])],
      milestoneFunctionValueBoolean: ['', Validators.compose([Validators.required])],
      milestoneFunctionValueEnum: ['', Validators.compose([Validators.required])],

    });

    this.milestoneFunctionValueFormControl = this.formTemplateMilestone.get("milestoneFunctionValue");
    this.milestoneFunctionValueMinFormControl = this.formTemplateMilestone.get("milestoneFunctionValueMin");
    this.milestoneFunctionValueMaxFormControl = this.formTemplateMilestone.get("milestoneFunctionValueMax");
    this.milestoneFunctionValueBooleanFormControl = this.formTemplateMilestone.get("milestoneFunctionValueBoolean");
    this.milestoneFunctionValueEnumFormControl = this.formTemplateMilestone.get("milestoneFunctionValueEnum");
    this.milestoneNameFormControl = this.formTemplateMilestone.get("milestoneName");
    this.planNameFormControl = this.formTemplateHeader.get("planName");
    this.injuryFormControl = this.formTemplateHeader.get("injure");

    this.planNameFormControl.setErrors({
      "notUnique": false
    });

    // this.dragulaService.setOptions('rehab', {
    //   invalid: (el, handle) => el.classList.contains('rehab-arrow')
    // });
  }

  ngOnInit(): void {

    this.initFuncstions();
    //this.initInjures();
    this.currentPlan = this.route.snapshot.data['plan'];
    this.injuresList = this.route.snapshot.data['inj'];

    if (this.currentPlan != undefined) {
      this.loadPlan();
    }
    else {
      this.currentPlan = new PlanDTO();
      this.calcPhaseDuration();
    }


  }

  loadPlan(): void {
    console.log(this.injuresList);
    this.planNameFormControl.setValue(this.currentPlan.name);
    this.injuryFormControl.setValue(this.injuresList.find(inj => inj.id == this.currentPlan.injury));
    this.isShowAllMilestones = true;
    this.isMilestoneEdit = false;
    this.calcPhaseDuration();
  }


  /* Создать новый шаг, если заголовок плана не создан, то создать его и вернуть id*/
  createNewMilestone() {

    if (this.currentPlan.id == null) {
      this.currentPlan.name = this.planNameFormControl.value;
      this.currentPlan.injury = this.injuryFormControl.value.id;

      this.templateService.createRehabilitationPlanHeader(this.currentPlan.injury, this.currentPlan.name).then(res => {
        this.currentPlan.id = res.json();
      });
    }

    this.currentFunctions = [];
    this.formTemplateMilestone.reset();
    this.isMilestoneEdit = true;
    this.currentPhaseForEditId = null;
  }

  /* Добавить новую функцию к шагу */
  public addNewFunction(): void {
    let phaseFunction = new FunctionModel();

    if (this.currentUnitTypeCode === INT_RANGE) {
      phaseFunction.valueNumeric = this.milestoneFunctionValueMinFormControl.value;
      //phaseFunction.valueNumeric = this.mFunctionValueMin;
      phaseFunction.valueNumericMax = this.milestoneFunctionValueMaxFormControl.value;
    }

    if (this.currentUnitTypeCode === INT) {
      phaseFunction.valueNumeric = this.milestoneFunctionValueFormControl.value;
    }

    if (this.currentUnitTypeCode === BOOLEAN) {
      phaseFunction.valueBoolean = this.milestoneFunctionValueBooleanFormControl.value;
    }

    if (this.currentUnitTypeCode === ENUM) {

      //phaseFunction.lookupValue = this.milestoneFunctionValueEnumFormControl.value;
      phaseFunction.lookupValue = this.mFunctionValueEnum;
      this.mFunctionValueEnumTarget = this.formTemplateMilestone.controls.milestoneFunctionValueEnum.value;
    }

    phaseFunction.function = this.formTemplateMilestone.get("milestoneFunction").value.id;
    phaseFunction.localizedName = this.formTemplateMilestone.get("milestoneFunction").value.localizedName;

    let functionFromList = this.functionsList.find(func => func.id === phaseFunction.function);
    phaseFunction.unitTypeCode = functionFromList.unitTypeCode;
    phaseFunction.localizedUnitName = functionFromList.localizedUnitName;
    phaseFunction.values = functionFromList.values;

    this.currentFunctions.push(phaseFunction);
  }

  /* изменить формат ввода значения для выбранной функции*/
  onChangeFunctionValue(): void {

    let phaseFunction = this.formTemplateMilestone.get("milestoneFunction").value;
    this.currentUnitTypeCode = phaseFunction.unitTypeCode;
    this.currentLocalizedUnitName = phaseFunction.localizedUnitName;
    this.currentFunctionsValuesList = phaseFunction.values;

    this.disableAllControls();

    if (this.currentUnitTypeCode === INT_RANGE) {
      this.milestoneFunctionValueMinFormControl.enable();
      this.milestoneFunctionValueMaxFormControl.enable();
    }

    if (this.currentUnitTypeCode === INT) {
      this.milestoneFunctionValueFormControl.enable()
    }

    if (this.currentUnitTypeCode === BOOLEAN) {
      this.milestoneFunctionValueBooleanFormControl.enable()
    }

    if (this.currentUnitTypeCode === ENUM) {
      this.milestoneFunctionValueEnumFormControl.enable()
    }
  }

  private disableAllControls() {
    this.milestoneFunctionValueBooleanFormControl.disable();
    this.milestoneFunctionValueMinFormControl.disable();
    this.milestoneFunctionValueMaxFormControl.disable();
    this.milestoneFunctionValueFormControl.disable();
    this.milestoneFunctionValueEnumFormControl.disable();
  }

  /*Удалить функцию из таблицы*/
  public deleteFunction(item): void {
    let index = this.currentFunctions.findIndex(d => d.function === item.function);
    this.currentFunctions.splice(index, 1);
  }

  /*сохранить или обновить шаг и набор функций в бд */
  public saveMilestone(): void {

    let currentPhase = new PhaseDTO();
    let milestoneDuration = this.formTemplateMilestone.get("milestoneDuration").value;
    let milestoneDurationValue = this.formTemplateMilestone.get("milestoneValueDuration").value;

    currentPhase.name = this.formTemplateMilestone.get("milestoneName").value;

    if (milestoneDuration === DAY) {
      currentPhase.days = milestoneDurationValue;
    }
    if (milestoneDuration === WEEK) {
      currentPhase.weeks = milestoneDurationValue;
    }

    this.currentFunctions.forEach(func => {
      let phaseFunction = new FunctionModel()
      phaseFunction.function = func.function;


      if (func.unitTypeCode == INT) {
        phaseFunction.valueNumeric = func.valueNumeric;
      }

      if (func.unitTypeCode == INT_RANGE) {
        phaseFunction.valueNumeric = func.valueNumeric;
        phaseFunction.valueNumericMax = func.valueNumericMax;
      }

      if (func.unitTypeCode == BOOLEAN) {
        phaseFunction.valueBoolean = func.valueBoolean;
      }

      if (func.unitTypeCode == ENUM) {
        phaseFunction.lookupValue = func.lookupValue;
      }

      currentPhase.goals.push(phaseFunction);
    });

    console.log(currentPhase);

    console.log("currentPhaseForEditId:" + this.currentPhaseForEditId);

    if (this.currentPhaseForEditId == null) {
      this.templateService.saveMilestone(currentPhase, this.currentPlan.id).then(res => {
        currentPhase.id = res.json();
        this.currentPlan.phases.push(currentPhase);
        this.calcPhaseDuration();
        console.log("created")
      });
    }

    if (this.currentPhaseForEditId != null) {
      this.templateService.updateMilestone(currentPhase, this.currentPlan.id, this.currentPhaseForEditId);
      let indexPhaseBeforeEdit = this.currentPlan.phases.findIndex(phase => phase.id === this.currentPhaseForEditId);
      this.currentPlan.phases.splice(indexPhaseBeforeEdit, 1);
      currentPhase.id = this.currentPhaseForEditId;
      this.currentPhaseForEditId = null;
      this.currentPlan.phases.push(currentPhase);
      this.calcPhaseDuration();
      console.log("updated")
    }

    console.log(this.currentPlan);

    this.isShowAllMilestones = true;
    this.isMilestoneEdit = false;
    this.isMilestoneSave = true;
    this.formTemplateMilestone.reset();

  }

  /* отменить редактирование шага*/
  cancelEditMileStone(): void {
    this.currentFunctions = [];
    this.formTemplateMilestone.reset();
    this.isMilestoneEdit = false;

    if (this.currentFunctions.length > 0) {
      this.isShowAllMilestones = true;
    }
  }

  /* Обновить список для справочника функций */
  initFuncstions(): void {
    this.templateService.getFunctions().then(res => {
      this.functionsList = (res.json() as Function[]).filter(func => {
        return func.unitTypeCode === INT ||
          func.unitTypeCode === INT_RANGE ||
          func.unitTypeCode === BOOLEAN ||
          func.unitTypeCode === ENUM
      });
    });
  }

  /* открыть созданный шаг для редактирования */
  openMilestoneForEdit(currentPhase): void {
    this.isMilestoneSave = false;
    this.currentUnitTypeCode = "";
    this.currentLocalizedUnitName = "";

    this.isMilestoneEdit = true;
    let phaseForEdit = this.currentPlan.phases.find(phase => phase.id == currentPhase.id);
    console.log(phaseForEdit);

    this.currentPhaseForEditId = phaseForEdit.id;
    this.milestoneNameFormControl.setValue(phaseForEdit.name);

    if (currentPhase.days > 0) {
      this.formTemplateMilestone.get("milestoneValueDuration").setValue(phaseForEdit.days);
      this.formTemplateMilestone.get("milestoneDuration").setValue(DAY);
    }

    if (currentPhase.weeks > 0) {
      this.formTemplateMilestone.get("milestoneValueDuration").setValue(phaseForEdit.weeks);
      this.formTemplateMilestone.get("milestoneDuration").setValue(WEEK);
    }

    let functionsForEdit = phaseForEdit.goals;

    functionsForEdit.forEach(funcForEdit => {
      let functionFromList = this.functionsList.find(func => func.id === funcForEdit.function);
      funcForEdit.localizedName = functionFromList.localizedName;
      funcForEdit.unitTypeCode = functionFromList.unitTypeCode;
      funcForEdit.values = functionFromList.values;
      funcForEdit.localizedUnitName = functionFromList.localizedUnitName;

    });

    this.currentFunctions = functionsForEdit;
  }

  /* сохранить имя, тип болезни и порядок шагов плана */
  public savePlan(): void {

    let request = new EditPlanArgumentsDTO();

    request.name = this.planNameFormControl.value;
    request.injury = this.injuryFormControl.value.id;

    this.currentPlan.phases.forEach(phase => {
      request.phases.push(phase.id);
    });

    this.templateService.saveRehabilitationPlan(this.currentPlan.id, request).then(res => {
      this.isMilestoneEdit = false;
      this.isShowAllMilestones = true;
    });

  }

  /*произвести пересчет длительности плана*/
  private calcPhaseDuration() {
    console.log("start calculation")
    if (this.currentPlan.phases.length > 0) {
      let phaseDurationDays = 0;
      this.currentPlan.phases.forEach(phase => {

        if (phase.days != undefined) {
          phaseDurationDays += phase.days;
        }

        if (phase.weeks != undefined) {
          phaseDurationDays += (phase.weeks * 7);
        }

      });
      phaseDurationDays = (phaseDurationDays / 7) * 100;

      this.phaseWeekDuration = Math.round(phaseDurationDays) / 100;
      console.log("end calculation")
    }
  }

  // ngOnDestroy() {
  //   this.dragulaService.destroy("rehab");
  // }

  backToList() {
    this.router.navigate(['/admin/templates'])
  }
  getValueEnum(el) {
    this.mFunctionValueEnum = el.id;
  }
  getVal(val) {
    let _val = val.split(' ');
    this.mFunctionValueMin = Number(_val[0]);
  }
  modalOpen(plan) {
    if (this.isMilestoneSave) {
      this.savePlan();
      this.modalRef = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
      this.modalRef.componentInstance.nameConfirm = this.modalsContentService.contentConfirmPlan.name;
      this.modalRef.componentInstance.eventConfirm = this.modalsContentService.contentConfirmPlan.event;
      this.modalRef.componentInstance.targetConfirm = plan;

      this.isMilestoneSave = false;
    } else {
      this.modalRef = this.modalService.open(ModalAttentionComponent);
      this.modalRef.componentInstance.planName = plan;
      this.modalRef.componentInstance.textAttention = this.modalsContentService.contentAttentionPlan.text;
      this.modalRef.componentInstance.btnAttention = this.modalsContentService.contentAttentionPlan.button;
      this.modalRef.result.then(() => { this.savePlan(); });

      this.isMilestoneSave = false;
    }

  }

  changeFieldDirty() {
    this.isFieldDirty = true;
  }

}




