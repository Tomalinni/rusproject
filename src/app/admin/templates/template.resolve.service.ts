import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';

import { TemplateService } from "./template.service";
import { PlanDTO } from "./models/planDTO";

@Injectable()
export class TemplateResolveService implements Resolve<PlanDTO> {

  constructor(private templateService: TemplateService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Promise<PlanDTO> {
    let id = +route.params['id'];
    return this.templateService.getRehabilitationPlan(id).then(planTemplate => {
      if (planTemplate) {
        return planTemplate;
      } else {
        this.router.navigate(['']);
        return false;
      }
    });
  }

}
