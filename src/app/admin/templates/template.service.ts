import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions } from "@angular/http";

import { environment } from "../../../environments/environment";
import { EditPlanArgumentsDTO } from "../../admin/templates/models/editPlanArgumentsDTO";
import { PlanDTO } from "./models/planDTO";
import { Injure } from "./models/injure.model";

/*
* Сервис для работы с планами реабилитации
*/
@Injectable()
export class TemplateService {

  private rmsRehabilitationUrl: String = "/rms-rehabilitation";

  constructor(private http: Http) {

  }

  getRehabilitationPlan(planId: number): Promise<PlanDTO> {
    return this.http
      .get(environment.baseUrl + this.rmsRehabilitationUrl + '/planTemplate/' + planId, this.accessToken())
      .toPromise()
      .then((response) => {
        return response.json() as PlanDTO;
      })
      .catch(this.handleError);
  }

  public isPlanNameExists(planName: string): Promise<boolean> {
    return this.http
      .get(environment.baseUrl + this.rmsRehabilitationUrl + '/planTemplate/search/byName?name=' + planName, this.accessToken())
      .toPromise()
      .then(response => {
        return true;
      })
      .catch(response => {
        return false;
      });
  }

  createRehabilitationPlanHeader(id, name) {
    return this.http.post(environment.baseUrl + this.rmsRehabilitationUrl + '/planTemplate/  ', { name: name, injury: id }, this.accessToken()).toPromise();
  }

  saveMilestone(body, planId) {
    return this.http.post(environment.baseUrl + this.rmsRehabilitationUrl + '/planTemplate/' + planId + '/phase/', body, this.accessToken()).toPromise();
  }

  updateMilestone(body, planId, phaseId) {
    return this.http.put(environment.baseUrl + this.rmsRehabilitationUrl + '/planTemplate/' + planId + '/phase/' + phaseId, body, this.accessToken()).toPromise();
  }

  public getInjury(): Promise<Injure[]> {
    return this.http
      .get(environment.baseUrl + this.rmsRehabilitationUrl + '/injury ', this.accessToken())
      .toPromise()
      .then((response) => {
        return response.json() as Injure[];
      });
  }

  getRehabByInjureList(injureId) {
    return this.http.get(environment.baseUrl + this.rmsRehabilitationUrl + '/planTemplate/search/byInjury?injury=' + injureId, this.accessToken()).toPromise();
  }

  getRehabList() {
    return this.http.get(environment.baseUrl + this.rmsRehabilitationUrl + '/planTemplate/', this.accessToken()).toPromise();
  }

  getFunctions() {
    return this.http.get(environment.baseUrl + this.rmsRehabilitationUrl + '/function', this.accessToken()).toPromise();
  }

  public saveRehabilitationPlan(planId: number, body: EditPlanArgumentsDTO): Promise<any> {
    return this.http.put(environment.baseUrl + this.rmsRehabilitationUrl + '/planTemplate/' + planId, body, this.accessToken()).toPromise();
  }

  private accessToken(): RequestOptions {
    let myHeaders = new Headers();
    let options = new RequestOptions({ headers: myHeaders });
    myHeaders.append("Content-Type", "application/json");
    let token = sessionStorage.getItem('token');
    myHeaders.append('Authorization', 'Bearer' + token);

    return new RequestOptions({ headers: myHeaders });
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
