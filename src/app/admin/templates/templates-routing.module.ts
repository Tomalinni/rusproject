import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultComponent } from '../../theme/pages/default/default.component';

import { RehabListComponent } from './rehab-list/rehab-list.component';
import { RehabilitationComponent } from "./rehabilitation/rehabilitation.component";
import { TemplateResolveService } from "./template.resolve.service";
import { InjuryResolveService } from "./injury.resolve.service";

const routes: Routes = [
  {
    path: '',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: RehabListComponent
      },
      {
        path: 'add-reha',
        component: RehabilitationComponent,
        resolve: {
          inj: InjuryResolveService
        }
      },
      {
        path: 'edit-reha/:id',
        component: RehabilitationComponent,
        resolve: {
          plan: TemplateResolveService
          , inj: InjuryResolveService
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplatesRoutingModule { }
