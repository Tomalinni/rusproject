import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TemplatesRoutingModule } from './templates-routing.module';
import { LayoutModule } from '../../theme/layouts/layout.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule, DropdownModule, OrderListModule, SelectButtonModule, SlideMenuModule, SpinnerModule } from 'primeng/primeng';
import { DataTableModule } from "primeng/components/datatable/datatable";
import { DragulaModule } from 'ng2-dragula';
import { TextMaskModule } from 'angular2-text-mask';

import { RehabListComponent } from './rehab-list/rehab-list.component';
import { RehabilitationComponent } from "./rehabilitation/rehabilitation.component";
import { TemplateService } from "app/admin/templates/template.service";
import { TemplateResolveService } from "./template.resolve.service";
import { InjuryResolveService } from "./injury.resolve.service";
import { PlanNameDirective } from "./plan-name.directive";

import { MilestonesComponent } from './components/milestones/milesones.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TemplatesRoutingModule,
    LayoutModule,
    SharedModule,
    DropdownModule,
    OrderListModule,
    SelectButtonModule,
    SlideMenuModule,
    SpinnerModule,
    DataTableModule,
    DragulaModule,
    TextMaskModule,
    NgbModule
  ],
  declarations: [
    RehabilitationComponent,
    RehabListComponent,
    PlanNameDirective,
    MilestonesComponent
  ],
  exports: [PlanNameDirective],
  providers: [
    TemplateService,
    TemplateResolveService,
    InjuryResolveService,
    PlanNameDirective
  ]
})
export class TemplatesModule { }
