
/* Класс для поиска по пользователям*/
export class UserSearchRequest {
  regDateLowBound: Date;
  regDateHighBound: Date;
  status: UserStatusCode;
  role: RoleCode;
}

export type RoleCode = 'SYSTEM_ADMINISTRATOR' | 'ORGANIZATION_ADMINISTRATOR' | 'MANAGER' |
  'CURATOR' | 'ASSISTANT' | 'DOCTOR' | 'PATIENT' | 'CARE_TAKER' | 'EXPERT';
export type UserStatusCode = 'ACTIVE' | 'NOT_ACTIVE' | 'LOCKED' | 'DELETED';
