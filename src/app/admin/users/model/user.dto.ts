import { UserStatusCode } from "./user-search-request";
import { LinkTypes } from "../user/add-user/add-user.component";

export class UserDto {
  lastName: string;
  firstName: string;
  middleName: string;
  birthDate: Date;
  email: string;
  sex: string;
  phone: string;
  roles: string[] = [];
  organizationLinks: LinkTypes[] = [];
}
