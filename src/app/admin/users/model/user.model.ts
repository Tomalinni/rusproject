import { UserStatusCode } from "./user-search-request";

export class UserModel {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
  middleName: string;
  creationDate: Date;
  statusName: string;
  statusCode: UserStatusCode;
  isSuperAdmin: boolean;
  fio: string;
}
