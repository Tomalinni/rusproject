import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response } from "@angular/http";
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { UserDto } from "./model/user.dto";
import { UserDTO } from "./user/models/user.model";
import { ROLES } from "./user/mock-data";
import { RoleDTO } from "./user/models/role.model";
import { Observable } from "rxjs/Observable";

/*
* Сервис для работы с пользователями
*/
@Injectable()
export class RolesService {

  private roleAPI: string = environment.baseUrl + "//rms-authorization/api/role";

  constructor(private http: HttpClient) { }

  public getRoles(): Promise<RoleDTO[]> {
    const options = { header: this.accessToken };

    return this.http
      .get(this.roleAPI, options)
      .map(response => response as RoleDTO[])
      .toPromise();

    // return Observable.of(ROLES).delay(700).toPromise();
  }

  private accessToken(): Headers {
    let myHeaders = new Headers();
    let options = new RequestOptions({ headers: myHeaders });
    myHeaders.append("Content-Type", "application/json");
    let token = sessionStorage.getItem('token');
    myHeaders.append('Authorization', 'Bearer' + token);
    return myHeaders;
  }

}
