import { Component, ErrorHandler, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Http, Response, Headers, } from "@angular/http";
import { environment } from "../../../../../environments/environment";
import { Router } from "@angular/router";
import { ModalComponent } from "../../../modal/modal.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalConfirmComponent } from "../../../modal/modal-confirm/modal-confirm.component";
import { AddNewAdminModel } from "../../../modal/modal-confirm/addNewAdminModel";
import { destroyView } from "@angular/core/src/view/view";
import { UsersService } from "../../users.service";


@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './add-admin-user.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class AddAdminUserComponent implements OnInit {
  errorStatus: any;
  requiredStatus: boolean = false;
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: Http, private router: Router,
    private modalService: NgbModal, private usersService: UsersService) {
  }

  return() {
    this.router.navigate(['admin/admin-users']);
  }

  ngOnInit() {
    let login = new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50), Validators.minLength(8), Validators.pattern("[a-zA-Z0-9_-]{1,}")]));
    let password = new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(50), Validators.pattern("[a-zA-Z0-9_-]{1,}"), CustomValidators.notEqualTo(login)]));
    let certainPassword = new FormControl('', CustomValidators.equalTo(password));

    this.form = this.formBuilder.group({
      lastName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern("[a-zA-Zа-яА-я]{1,}")])],
      login: login,
      firstName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern("[a-zA-Zа-яА-я]{1,}")])],
      password: password,
      middleName: ['', Validators.compose([Validators.maxLength(50), Validators.pattern("[a-zA-Zа-яА-я]{1,}")])],
      repeatPass: certainPassword
    });
  }

  getRequired(isRequired: boolean) {
    return this.requiredStatus = isRequired;
  }

  submitForm(form) {
    this.usersService.createSysAdmin(form.login._value,
      form.firstName._value,
      form.lastName._value,
      form.middleName._value,
      form.password._value)
      .then(res => {
        this.modalService.open(AddNewAdminModel, { size: 'sm' });
        this.router.navigate(['admin/admin-users']);
      })
      .catch(err => {
        this.errorStatus = "Логин уже существует"
      })
    this.errorStatus = '';
  }
}
