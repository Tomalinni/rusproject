import { Component, Input, OnInit, ViewEncapsulation, AfterViewInit, Injectable } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalComponent } from '../../modal/modal.component';
import { UserService } from "../../../auth/_services/user.service";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { AuthenticationService } from "../../../auth/_services/authentication.service";
import { ModalConfirmComponent } from "../../modal/modal-confirm/modal-confirm.component";
import { ModalsService } from "../../../_services/modals.service";
import { User } from "../../../auth/_models/user";

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./user-admin.component.html",
  encapsulation: ViewEncapsulation.None,
})
export class UserAdminComponent implements OnInit, AfterViewInit {
  private modalRef;
  rowFrom: any = 1;
  rowUntil: any = 10;
  dtLength: any;
  filteredLength: any;
  isFiltered: any;
  datatable: any;
  currentUser: User;

  constructor(
    private auth: AuthenticationService,
    private http: Http,
    private _script: ScriptLoaderService,
    private modalService: NgbModal,
    private userService: UserService,
    private modalsContentService: ModalsService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ruTranslate = { active: "Активен", locked: "Заблокирован", not_active: "Неактивен", deleted: "Удален" };
  dataSource; //for table


  ngOnInit() {
    this.userService.getSysAdminUsers().then(result => (this.dataSource = result, this.renameField()));
  }

  ngAfterViewInit() {
  }

  renameField() {
    for (let i = 0; i < this.dataSource.length; i++) {
      if (this.dataSource[i].status_code == "ACTIVE") {
        this.dataSource[i].statusTranslate = this.ruTranslate.active;
        this.dataSource[i].class = 'success m-badge--wide'
      }
      if (this.dataSource[i].status_code == "LOCKED" || this.dataSource[i].status_code == "DELETED" || this.dataSource[i].status_code == "LOCKED_INACTIVE") {
        this.dataSource[i].statusTranslate = this.ruTranslate.locked;
        this.dataSource[i].class = 'danger m-badge--wide'
      }
      if (this.dataSource[i].status_code == "NOT_ACTIVE") {
        this.dataSource[i].statusTranslate = this.ruTranslate.not_active;
        this.dataSource[i].class = 'metal m-badge--wide'
      }
      this.dataSource[i].creationDate = this.dataSource[i].registrationDate;
    }
    console.log('this.dataSource: ', this.dataSource);
  }

  modalOpen(id, fio, item) {
    if (this.currentUser.id !== id) {
      localStorage.setItem('id', id);
      localStorage.setItem('fio', fio);
      this.modalRef = this.modalService.open(ModalComponent);
    }
  }

  unblockUser(item) {
    // Смотрим какой статус у пользователя перед разблокировкой (LOCKED или LOCKED_INACTIVE),
    // чтобы сразу после разблокировки присвоить ему нужный статус без перезагрузки страницы (ACTIVE или INACTIVE);
    const itemStatusCode = item.status_code;

    this.userService.unblockAdministrator(item.id).then(
      result => {
        // Если ранее юзер был заблокирован в статусе ACTIVE;
        if (itemStatusCode == "LOCKED") {
          // Делаем статус ACTIVE;
          item.statusTranslate = this.ruTranslate.active;
          item.class = 'success m-badge--wide';
          item.status_code = 'ACTIVE';
        }
        // Если ранее юзер был заблокирован в статусе INACTIVE;
        if (itemStatusCode == "LOCKED_INACTIVE") {
          // Делаем статус INACTIVE;
          item.statusTranslate = this.ruTranslate.not_active;
          item.class = 'metal m-badge--wide'
          item.status_code = 'INACTIVE';
        }
        this.confirmUnblock();
      },
      err => {
        console.log('err: ', err);
      }
    );
  }

  confirmUnblock() {
    const modalRef = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
    modalRef.componentInstance.nameConfirm = this.modalsContentService.contentConfirmUnblockUser.text;
  }

  countRows(event) {
    this.rowFrom = (event.first == 0) ? 1 : event.first + 1;
    this.dtLength = this.datatable.value.length;
    let _rowUntil = this.rowFrom + event.rows - 1;

    if (_rowUntil > this.dtLength) {
      return this.rowUntil = this.dtLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  countRowsFilter(event) {
    this.rowFrom = 1;
    let _rowUntil = this.rowFrom + this.datatable.rows - 1;
    this.dtLength = this.datatable.value.length;
    this.rowUntil = event.filteredValue.length;
    this.filteredLength = event.filteredValue.length;

    if (this.filteredLength == 0) {
      this.isFiltered = 'empty';
    } else if (this.filteredLength == 1) {
      this.isFiltered = 'one';
    } else if (this.dtLength == this.filteredLength) {
      this.isFiltered = '';
    } else {
      this.isFiltered = 'fromTo';
    }

    if (_rowUntil > this.filteredLength) {
      return this.rowUntil = this.filteredLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }

  getDt(el) {
    return this.datatable = el;
  }
}
