import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DropdownModule, OrderListModule, SelectButtonModule } from 'primeng/primeng';
import { DragulaModule } from 'ng2-dragula';
import { TextMaskModule } from 'angular2-text-mask';

import { UserAdminComponent } from './user-admin.component';
import { LayoutModule } from '../../../theme/layouts/layout.module';
import { DefaultComponent } from '../../../theme/pages/default/default.component';
import { SharedModule } from "primeng/primeng";
import { DataTableModule } from "primeng/components/datatable/datatable";
import { AddAdminUserComponent } from "./add-user/add-admin-user.component";
import { SlideMenuModule, MenuItem } from 'primeng/primeng';
import { UsersService } from "../users.service";

const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": UserAdminComponent
      },
      {
        "path": "add-admin",
        "component": AddAdminUserComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    NgbModule,
    CommonModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    SharedModule,
    DropdownModule,
    SelectButtonModule,
    OrderListModule,
    DragulaModule,
    TextMaskModule,
    SlideMenuModule
  ], exports: [
    RouterModule,
  ], declarations: [
    AddAdminUserComponent,
    UserAdminComponent
  ],
  providers: [UsersService]
})
export class UserAdminModule {


}

