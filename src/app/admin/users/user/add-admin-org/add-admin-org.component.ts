import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { PrimengService } from "../../../../_services/primeng.service";
import { MainInformationModel } from "../../../organisations/model/main-information.model";
import { OrganizationsService } from "../../../organisations/organizations.service";
import { ActivatedRoute, Router } from "@angular/router";
import { OrganizationModel } from "../../../organisations/model/organization.model";
import { LinkTypes } from "../add-user/add-user.component";
import { UserDto } from "../../model/user.dto";
import { UsersService } from "../../users.service";
import { ModalConfirmComponent } from "../../../modal/modal-confirm/modal-confirm.component";
import { Observable } from "rxjs/Observable";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalsService } from "../../../../_services/modals.service";
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './add-admin-org.component.html',
  styles: []
})
export class AddAdminOrgComponent implements OnInit {

  errorStatus: any;
  requiredStatus: boolean = false;
  form: FormGroup;
  ru: any;
  org: OrganizationModel;
  maxNameLength: number = 100;

  private modalRef;
  private timer;

  countryList: any;
  maskList: any;
  country: string = 'RF';
  mask: string;
  placeholderMask: string;
  private sub: any;
  minDate: Date;
  maxDate: Date;

  constructor(
    private formBuilder: FormBuilder,
    private primengService: PrimengService,
    private service: OrganizationsService,
    private route: ActivatedRoute,
    private userService: UsersService,
    private modalService: NgbModal,
    private router: Router,
    private modalsContentService: ModalsService,

  ) {

    this.org = new OrganizationModel();
    this.org.mainInformation = new MainInformationModel();
    this.minDate = primengService.setMinDate();
    this.maxDate = primengService.setMaxDate();


    this.form = this.formBuilder.group({
      lastName: ['', Validators.compose([Validators.required, Validators.maxLength(this.maxNameLength), Validators.pattern("^[а-яА-ЯёЁa-zA-Z]+$")])],
      firstName: ['', Validators.compose([Validators.required, Validators.maxLength(this.maxNameLength), Validators.pattern("^[а-яА-ЯёЁa-zA-Z]+$")])],
      middleName: ['', Validators.compose([Validators.maxLength(this.maxNameLength), Validators.pattern("^[а-яА-ЯёЁa-zA-Z]+$")])],
      birthDate: ['', Validators.compose([Validators.required, CustomValidators.minDate(this.minDate), CustomValidators.maxDate(this.maxDate)])],
      phone: ['', Validators.compose([Validators.required])],
      phoneCountry: [''],
      email: ['', Validators.compose([Validators.required, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])],
      sex: ['', Validators.compose([Validators.required])],
      organization: [{ value: '', disabled: true }]
    });

    this.ru = primengService.ru;
    this.countryList = primengService.countryList;
    this.maskList = primengService.maskList;
  }

  public dateCheck(fControl: FormControl) {
    console.log(new Date(fControl.value).getFullYear());
    let isValid = fControl.value.getFullYear() < 2000 && fControl.value.getFullYear() > 1989;
    return isValid ? null : { 'wrongDates': true }
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = +params['id'];
      this.service.getOrganization(id).then(organization => {
        this.org = organization;
        this.form.get('organization').setValue(this.org.mainInformation.fullName);
      });
    });

    this.mask = this.maskList[this.country].mask;
    this.placeholderMask = this.maskList[this.country].placeholder;
  }
  getRequired(isRequired: boolean) {
    return this.requiredStatus = isRequired;
  }
  getFieldCountry(fld) {
    let control = fld.name;

    if (control == 'phoneCountry') {
      this.country = fld.value.value;
      this.mask = this.maskList[this.country].mask;
      this.placeholderMask = this.maskList[this.country].placeholder;
    }
  }

  submitForm({ value, valid }: { value: UserDto, valid: boolean }) {

    let linkTypeOrganization = new LinkTypes();
    linkTypeOrganization.organizationId = this.org.id;
    linkTypeOrganization.linkTypeIds = [2];

    let createUser = new UserDto();

    createUser.birthDate = value.birthDate;
    createUser.email = value.email;
    createUser.firstName = value.firstName;
    createUser.lastName = value.lastName;
    createUser.sex = value.sex;
    createUser.middleName = value.middleName;
    createUser.phone = value.phone;
    createUser.roles.push('ORGANIZATION_ADMINISTRATOR');
    createUser.organizationLinks.push(linkTypeOrganization);

    this.userService.saveUser(createUser).then(result => {
      this.showDoneCreatingModal();
    });
  }

  public showDoneCreatingModal() {
    this.modalRef = this.modalService.open(ModalConfirmComponent, { size: 'sm' });
    this.modalRef.componentInstance.nameConfirm = this.modalsContentService.content.confirm.addAdminOrg.name;
    this.modalRef.componentInstance.targetConfirm = this.form.get("firstName").value + ' ' + this.form.get("lastName").value;
    this.modalRef.componentInstance.eventConfirm = this.modalsContentService.content.confirm.addAdminOrg.event;
    this.timer = Observable.timer(2000, 1000);
    this.timer.subscribe(t => {
      this.modalRef.close();
      this.goToOrganizations();
      this.timer.stop();
    });
  }

  public goToOrganizations(): void {
    this.router.navigate(['admin/organisations'])
  }

}
