import { Component, ErrorHandler, OnInit, ViewEncapsulation } from '@angular/core';
import { Http, Response, Headers, } from "@angular/http";
import { environment } from "../../../../../environments/environment";
import { Router } from "@angular/router";
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { CustomValidators } from 'ng2-validation';
import { PrimengService } from "../../../../_services/primeng.service";

@Component({
  selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
  templateUrl: './add-user.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class AddUserComponent implements OnInit {
  errorStatus: any;
  requiredStatus: boolean = false;
  form: FormGroup;
  ru: any;
  maxNameLength: number = 100;
  minDate: Date;
  maxDate: Date;

  public rolesComponent: Array<RoleValues> = null;

  constructor(
    private formBuilder: FormBuilder,
    private http: Http,
    private router: Router,
    private primengService: PrimengService) {

    this.ru = primengService.ru;
    this.minDate = primengService.setMinDate();
    this.maxDate = primengService.setMaxDate();
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      lastName: ['', Validators.compose([Validators.required, Validators.maxLength(this.maxNameLength), Validators.pattern("^[а-яА-ЯёЁa-zA-Z]+$")])],
      firstName: ['', Validators.compose([Validators.required, Validators.maxLength(this.maxNameLength), Validators.pattern("^[а-яА-ЯёЁa-zA-Z]+$")])],
      middleName: ['', Validators.compose([Validators.maxLength(this.maxNameLength), Validators.pattern("^[а-яА-ЯёЁa-zA-Z]+$")])],
      sex: ['', Validators.compose([Validators.required])],
      birthDate: ['', Validators.compose([Validators.required, CustomValidators.minDate(this.minDate), CustomValidators.maxDate(this.maxDate)])],
      phone: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      roles: [this.formBuilder.array([]), Validators.compose([Validators.required])]
    });

    let headers: Headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append('Authorization', 'Bearer' + sessionStorage.getItem('token') + '');
    this.http
      .get(environment.baseUrl + '/rms-authorization/api/role', { headers: headers })
      .map((res: Response) => <RoleValues[]>res.json())
      .subscribe(roles => {
        this.rolesComponent = roles;
        this.initForm();
      });

  }

  initForm() {
    let allRolesFormArray: FormArray = new FormArray([]);

    for (let i = 0; i < this.rolesComponent.length; i++) {
      let fg = new FormGroup({});
      fg.addControl(this.rolesComponent[i].code, new FormControl(false))
      allRolesFormArray.push(fg)
    }
    this.form.setControl("roles", allRolesFormArray);
  }

  get roles(): FormArray {
    return this.form.get('roles') as FormArray;
  };

  getRequired(isRequired: boolean) {
    return this.requiredStatus = isRequired;
  }

  submitForm(form) {

    let headers: Headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append('Authorization', 'Bearer' + sessionStorage.getItem('token') + '');

    let userRoles = [];

    this.form.get("roles").value.forEach(function logArrayElements(element, index, array) {


      if (element.SYSTEM_ADMINISTRATOR == true) {
        userRoles.push('SYSTEM_ADMINISTRATOR');
      }
      if (element.ORGANIZATION_ADMINISTRATOR == true) {
        userRoles.push('ORGANIZATION_ADMINISTRATOR');
      }
      if (element.MANAGER == true) {
        userRoles.push('MANAGER');
      }
      if (element.CURATOR == true) {
        userRoles.push('CURATOR');
      }
      if (element.DOCTOR == true) {
        userRoles.push('DOCTOR');
      }
      if (element.PATIENT == true) {
        userRoles.push('PATIENT');
      }
      if (element.CARE_TAKER == true) {
        userRoles.push('CARE_TAKER');
      }
      if (element.EXPERT == true) {
        userRoles.push('EXPERT');
      }
    }
    );

    let linkTypes = [];

    let linkTypeOrganization1 = new LinkTypes();
    linkTypeOrganization1.organizationId = 10;
    linkTypeOrganization1.linkTypeIds = [1, 2, 3, 4, 5];
    linkTypes.push(linkTypeOrganization1);

    let linkTypeOrganization2 = new LinkTypes();
    linkTypeOrganization2.organizationId = 11;
    linkTypeOrganization2.linkTypeIds = [2, 3];
    linkTypes.push(linkTypeOrganization2);

    this.http.post(environment.baseUrl + '/rms-authorization/api/user', {
      lastName: form.lastName._value,
      firstName: form.firstName._value,
      middleName: form.middleName._value,
      birthDate: form.birthDate._value,
      email: form.email._value,
      sex: form.sex._value,
      phone: form.phone._value,
      roles: userRoles,
      organizationLinks: linkTypes
    }, { headers: headers })
      .subscribe(Response => {
        if (Response.status == 201) { this.router.navigate(['admin/users']) };
      },
      err => { if (err.status == 400) { this.errorStatus = "Логин уже существует" } }
      )
    this.errorStatus = '';
  }

}

export class RoleValues {
  id: number;
  value: string;
  code: string;
}


export class LinkTypes {
  organizationId: number;
  linkTypeIds: number[];
}
