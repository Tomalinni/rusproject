import { UserDTO } from "./models/user.model";
import { RoleDTO } from "./models/role.model";

export const USERS_ADMIN_CLINIC: UserDTO[] = [
  {
    url: null,
    fio: 'Иванов Иван Иванович1',
    organization: 'Городская поликлиника № 2 ЮАО',
    address: 'Москва, ул. Александра Солженицына, д. 5, стр.1',
    login: 'Login12452',
    status: 1,
    status_code: 'ACTIVE',
    registrationDate: 1514460419651
    // date: 1514460419651
  },
  {
    url: null,
    fio: 'Иванов Иван Иванович4',
    organization: 'Городская поликлиника № 2 ЮАО',
    address: 'Москва, ул. Александра Солженицына, д. 5, стр.1',
    login: 'host',
    status: 1,
    status_code: 'ACTIVE',
    registrationDate: 1514460419651
    // date: 1514460419651
  },
  {
    url: null,
    fio: 'Иванов Иван Иванович2',
    organization: 'Городская поликлиника № 2 ЮАО',
    address: 'Москва, ул. Александра Солженицына, д. 5, стр.1',
    login: 'host',
    status: 1,
    status_code: 'ACTIVE',
    registrationDate: 1498739299000
    // date: 1498739299000
  },
  {
    url: null,
    fio: 'Иванов Иван Иванович3',
    organization: 'Городская поликлиника № 2 ЮАО',
    address: 'Москва, ул. Александра Солженицына, д. 5, стр.1',
    login: 'Login124',
    status: 1,
    status_code: 'LOCKED',
    registrationDate: 1514460419653
    // date: 1514460419653
  }
];

export const USERS_DOCTOR: UserDTO[] = [
  {
    url: null,
    fio: 'Петрова Юлия',
    organization: 'Городская поликлиника № 2 ЮАО',
    address: 'Москва, ул. Александра Солженицына, д. 5, стр.1',
    login: 'Login12452',
    status: 1,
    status_code: 'ACTIVE',
    registrationDate: 1514460419658
    // date: 1514460419658
  },
  {
    url: null,
    fio: 'Петрова Юлия',
    organization: 'Городская поликлиника № 2 ЮАО',
    address: 'Москва, ул. Александра Солженицына, д. 5, стр.1',
    login: 'Login12452',
    status: 1,
    status_code: 'ACTIVE',
    registrationDate: 1498739299000
    // date: 1498739299000
  }
];

export const ROLES: RoleDTO[] = [

  {
    id: 0,
    code: 'ORGANIZATION_ADMINISTRATOR',
    value: 'Администраторы клиники'
  },
  {
    id: 1,
    code: 'DOCTOR',
    value: 'Врачи'
  }

];
