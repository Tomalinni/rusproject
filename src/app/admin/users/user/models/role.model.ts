export class RoleDTO {
  id: number;
  code: string;
  value: string;
}
