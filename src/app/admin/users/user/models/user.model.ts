export class UserDTO {
  url: string;
  fio: string;
  organization: string;
  address: string;
  login: string;
  registrationDate: number;
  status: number;
  status_code: string;
}
