import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions } from "@angular/http";
import { UserDTO } from "../models/user.model";
import { RoleDTO } from "../models/role.model";
import { ROLES } from "../mock-data";
import { environment } from "../../../../../environments/environment";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/debounceTime';

/*
* Сервис для работы с пользователями
*/
@Injectable()
export class CommonUsersService {

  constructor(private http: Http) { }

  getUsers(role: any, registrationDate?: number, status?: number): Promise<UserDTO[]> {
    const params = {
      role: role
    };
    return this.http
      .post(environment.baseUrl + '/rms-authorization/api/user/search', params, this.accessToken())
      .map(response => {
        let users = response.json();
        users = users.map(user => {
          user.registrationDate = new Date(user.registrationDate).getTime();
          return user;
        });
        return users as UserDTO[];
      })
      .do(log => console.log(log))
      .toPromise();
  }

  getRoles(): Promise<RoleDTO[]> {
    return Observable.of(ROLES).delay(700).toPromise();
  }

  private accessToken(): RequestOptions {
    let myHeaders = new Headers();
    let options = new RequestOptions({ headers: myHeaders });
    myHeaders.append("Content-Type", "application/json");
    let token = sessionStorage.getItem('token');
    myHeaders.append('Authorization', 'Bearer' + token);

    return new RequestOptions({ headers: myHeaders });
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
