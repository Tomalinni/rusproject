import { Component, Input, OnInit, ViewChild, AfterViewChecked } from '@angular/core';
import { DataTable } from "primeng/components/datatable/datatable";
import { PrimengService } from "../../../../_services/primeng.service";
import { UsersService } from "../../users.service";
import { RolesService } from "../../roles.service";


const translates = {
  'ACTIVE': {
    lng: {
      ru: "Активен"
    },
    class: "success"
  },
  'LOCKED': {
    lng: {
      ru: "Заблокирован"
    },
    class: "danger"
  },
  'NOT_ACTIVE': {
    lng: {
      ru: "Не активен"
    },
    class: "metal"
  },
  'DELETED': {
    lng: {
      ru: "Удален"
    },
    class: "danger"
  },
  'LOCKED_INACTIVE':
  {
    lng: {
      ru: "Заблокирован"
    },
    class: "danger"
  }
};

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements AfterViewChecked, OnInit {
  @ViewChild('datatable') dt: DataTable;
  dataSource;
  roles: any[];
  loading: boolean;
  regDate: any;
  ru: any;

  conditions = [
    { label: 'Не выбрано', value: null },
    { label: 'Активен', value: 'Активен' },
    { label: 'Заблокирован', value: 'Заблокирован' },
    { label: 'Не активен', value: 'Не активен' }
  ];

  constructor(
    private usersService: UsersService,
    public pimengService: PrimengService,
    private roleService: RolesService
  ) { }

  // Custom function filter
  between(value: any, filter: [any, any]): boolean {
    if (filter[0] === null) return true;
    if (value > filter[0] && value < filter[1]) return true;
    return false;
  };

  ngAfterViewChecked() {
    // Add custom dataTable filter constraints
    if (this.dt !== undefined) {
      const customFilterConstraints = this.dt.filterConstraints;
      customFilterConstraints['between'] = this.between;
      this.dt.filterConstraints = customFilterConstraints;
    }
  }

  ngOnInit() {
    this.ru = this.pimengService.ru;
    this.roleService.getRoles().then(roles => {
      this.roles = roles.map((role, index) => {
        return {
          name: role.value,
          code: role.code,
          active: (index === 0) ? true : false
        }
      })
    })
    this.loadUsers('ORGANIZATION_ADMINISTRATOR');
  }

  filterDate(dt: DataTable) {
    if (this.regDate && this.regDate[0] && this.regDate[1]) {
      dt.filter([new Date(this.regDate[0]).getTime(), new Date(this.regDate[1]).getTime()], 'registrationDate', 'between');
    } else if (this.regDate && this.regDate[0]) {
      let day = 24 * 3600 * 1000;
      let date = new Date(this.regDate[0]).getTime();
      dt.filter([date, date + day], 'registrationDate', 'between')
    }
  }

  clearDate(dt: DataTable) { dt.filter([null, null], 'registrationDate', 'between') }

  loadUsers(roleCode) {
    this.loading = true;
    this.usersService.getUsers(roleCode).then(users => {
      this.dataSource = users.map((user: any) => {
        let code = user.status_code;
        user.class = translates[code].class;
        user.statusText = translates[code].lng.ru;
        return user;
      });
      this.loading = false;
    });
  }

  activeRole(role) {
    this.roles = this.roles.map(rl => {
      if (rl.code === role.code)
        rl.active = true;
      else
        rl.active = false;
      return rl;
    });
    this.loadUsers(role.code);
  }

  rowFrom: any = 1;
  rowUntil: any = 10;
  dtLength: any;
  filteredLength: any;
  isFiltered: any;
  datatable: any;

  countRows(event) {
    this.rowFrom = (event.first == 0) ? 1 : event.first + 1;
    this.dtLength = this.datatable.value.length;
    let _rowUntil = this.rowFrom + event.rows - 1;

    if (_rowUntil > this.dtLength) {
      return this.rowUntil = this.dtLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  countRowsFilter(event) {
    this.rowFrom = 1;
    let _rowUntil = this.rowFrom + this.datatable.rows - 1;
    this.dtLength = this.datatable.value.length;
    this.rowUntil = event.filteredValue.length;
    this.filteredLength = event.filteredValue.length;

    if (this.filteredLength == 0) {
      this.isFiltered = 'empty';
    } else if (this.filteredLength == 1) {
      this.isFiltered = 'one';
    } else if (this.dtLength == this.filteredLength) {
      this.isFiltered = '';
    } else {
      this.isFiltered = 'fromTo';
    }

    if (_rowUntil > this.filteredLength) {
      return this.rowUntil = this.filteredLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  getDt(el) {
    return this.datatable = el;
  }

}
