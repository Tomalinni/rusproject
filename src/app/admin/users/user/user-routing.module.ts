import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultComponent } from '../../../theme/pages/default/default.component';
import { UserComponent } from './user.component';
import { AddUserComponent } from "./add-user/add-user.component";
import { UsersAdminClinicComponent } from './users-admin-clinic/users-admin-clinic.component';
import { UsersDoctorComponent } from './users-doctor/users-doctor.component';
import { UsersPatientComponent } from './users-patient/users-patient.component';
import { UsersExpertComponent } from './users-expert/users-expert.component';
import { UsersTrainerComponent } from './users-trainer/users-trainer.component';
import { UserListComponent } from "./user-list/user-list.component";
import { AddAdminOrgComponent } from "./add-admin-org/add-admin-org.component";

const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        // "path": "",
        // "component": UserComponent,
        // "children": [
        //   {
        //     "path": "",
        //     "redirectTo": "admin-clinic",
        //     "pathMatch": "full"
        //   },
        //   {
        //     "path": "admin-clinic",
        //     "component": UsersAdminClinicComponent
        //   },
        //   {
        //     "path": "doctor",
        //     "component": UsersDoctorComponent
        //   },
        //   {
        //     "path": "patient",
        //     "component": UsersPatientComponent
        //   },
        //   {
        //     "path": "expert",
        //     "component": UsersExpertComponent
        //   },
        //   {
        //     "path": "trainer",
        //     "component": UsersTrainerComponent
        //   }
        //   ,
        //   {
        //     "path": "test-search-list",
        //     "component": UserListComponent
        //   }
        // ]
        "path": "",
        "component": UserListComponent
      },
      {
        "path": "add-user",
        "component": AddUserComponent
      },
      {
        path: 'add-admin/:id',
        component: AddAdminOrgComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
