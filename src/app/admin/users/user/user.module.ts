import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule, DropdownModule, InputMaskModule, CalendarModule, ToggleButtonModule, CheckboxModule } from 'primeng/primeng';
import { DataTableModule } from "primeng/components/datatable/datatable";
import { LayoutModule } from '../../../theme/layouts/layout.module';

import { UserComponent } from './user.component';
import { AddUserComponent } from "./add-user/add-user.component";
import { UsersAdminClinicComponent } from './users-admin-clinic/users-admin-clinic.component';
import { UsersDoctorComponent } from './users-doctor/users-doctor.component';
import { UsersPatientComponent } from './users-patient/users-patient.component';
import { UsersExpertComponent } from './users-expert/users-expert.component';
import { UsersTrainerComponent } from './users-trainer/users-trainer.component';
import { UserListComponent } from "./user-list/user-list.component";
import { CommonUsersService } from "./services/user.service";
import { UsersService } from "../users.service";
import { OrganizationsService } from "../../organisations/organizations.service";
import { AddAdminOrgComponent } from "./add-admin-org/add-admin-org.component";
import { OrganisationsModule } from "../../organisations/organisations.module";
import { RolesService } from "../roles.service";
import { CustomFormsModule } from 'ng2-validation';

@NgModule({
  imports: [
    NgbModule,
    UserRoutingModule,
    CommonModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    SharedModule,
    CheckboxModule,
    DropdownModule,
    InputMaskModule,
    CalendarModule,
    ToggleButtonModule,
    OrganisationsModule,
    CustomFormsModule
  ],
  declarations: [AddAdminOrgComponent, UserListComponent, UserComponent, AddUserComponent, UsersAdminClinicComponent, UsersDoctorComponent, UsersPatientComponent, UsersExpertComponent, UsersTrainerComponent],
  providers: [
    UsersService, RolesService
  ]
})
export class UserModule { }





