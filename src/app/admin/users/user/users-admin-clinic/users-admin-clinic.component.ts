import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SelectItem } from 'primeng/primeng';

import { ModalComponent } from '../../../modal/modal.component'
import { UserService } from "../../../../auth/_services/user.service";
import { PrimengService } from "../../../../_services/primeng.service";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { AuthenticationService } from "../../../../auth/_services/authentication.service";
import { User } from "../../../../auth/_models/user";

@Component({
  selector: '.tab-pane.tab-pane--admin-clinic',
  templateUrl: './users-admin-clinic.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class UsersAdminClinicComponent implements OnInit {

  private modalRef;
  conditions: SelectItem[];
  condition: string;
  orgs: SelectItem[];
  org: string;
  regDate: any;
  ru: any;
  toggle: boolean = false;
  rowFrom: any = 1;
  rowUntil: any = 10;
  dtLength: any;
  filteredLength: any;
  isFiltered: any;
  datatable: any;
  currentUser: User;

  constructor(
    private auth: AuthenticationService,
    private http: Http,
    private _script: ScriptLoaderService,
    private modalService: NgbModal,
    private userService: UserService,
    private primengService: PrimengService) {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.conditions = [
      { label: 'Все', value: 'Все' },
      { label: 'Активен', value: 'Активен' },
      { label: 'Заблокирован', value: 'Заблокирован' },
      { label: 'Неактивен', value: 'Неактивен' },
      { label: 'Удален', value: 'Удален' },
    ];
    this.orgs = [
      { label: 'Городская поликлиника № 2 ЮАО', value: 1 },
      { label: 'Городская поликлиника № 5 ЦАО', value: 2 },
      { label: 'Московский научно-практический центр наркологии Реабилитационный центр', value: 3 },
      { label: 'Городская клиническая больница имени В.П. Демихова', value: 4 },
    ];

    this.ru = primengService.ru;
  }

  ruTranslate = { active: "Активен", locked: "Заблокирован", not_active: "Неактивен", deleted: "Удален" }
  dataSource; //for table

  ngOnInit() {
    //this.userService.getSysAdminUsers().then(result => (this.dataSource = result.json(), this.renameField()), err => (this.auth.logout()));
    this.userService.getSysAdminUsers().then(result => (this.dataSource = result, this.renameField()));

  }
  renameField() {
    for (let i = 0; i < this.dataSource.length; i++) {
      if (this.dataSource[i].statusCode == "ACTIVE") {
        this.dataSource[i].statusTranslate = this.ruTranslate.active;
        this.dataSource[i].class = 'success m-badge--wide'
      }
      if (this.dataSource[i].statusCode == "LOCKED" || this.dataSource[i].statusCode == "DELETED") {
        this.dataSource[i].statusTranslate = this.ruTranslate.locked;
        this.dataSource[i].class = 'danger m-badge--wide'
      }
      if (this.dataSource[i].statusCode == "NOT_ACTIVE") {
        this.dataSource[i].statusTranslate = this.ruTranslate.not_active;
        this.dataSource[i].class = 'metal m-badge--wide'
      }
    }
  }

  modalOpen(id, fio) {
    if (this.currentUser.id !== id) {
      this.modalRef = this.modalService.open(ModalComponent);
      localStorage.setItem('id', id);
      localStorage.setItem('fio', fio);
    }
  }

  countRows(event) {
    this.rowFrom = (event.first == 0) ? 1 : event.first + 1;
    this.dtLength = this.datatable.value.length;
    let _rowUntil = this.rowFrom + event.rows - 1;

    if (_rowUntil > this.dtLength) {
      return this.rowUntil = this.dtLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  countRowsFilter(event) {
    this.rowFrom = 1;
    let _rowUntil = this.rowFrom + this.datatable.rows - 1;
    this.dtLength = this.datatable.value.length;
    this.rowUntil = event.filteredValue.length;
    this.filteredLength = event.filteredValue.length;

    if (this.filteredLength == 0) {
      this.isFiltered = 'empty';
    } else if (this.filteredLength == 1) {
      this.isFiltered = 'one';
    } else if (this.dtLength == this.filteredLength) {
      this.isFiltered = '';
    } else {
      this.isFiltered = 'fromTo';
    }

    if (_rowUntil > this.filteredLength) {
      return this.rowUntil = this.filteredLength;
    } else {
      return this.rowUntil = _rowUntil;
    }
  }
  getDt(el) {
    return this.datatable = el;
  }

  resetFilter(value, dt) {
    if (value == 'Все') {
      dt.reset();
    }
  }
}
