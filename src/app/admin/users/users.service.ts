import { Injectable } from "@angular/core";
import { Headers, RequestOptions } from "@angular/http";
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { UserDto } from "./model/user.dto";
import { UserDTO } from "./user/models/user.model";

/*
* Сервис для работы с пользователями
*/
@Injectable()
export class UsersService {

  private userAPI: string = environment.baseUrl + "/rms-authorization/api/user";

  constructor(private http: HttpClient) { }

  public saveUser(userDto: UserDto): Promise<number> {
    return this.http
      .post(this.userAPI, userDto)
      .toPromise()
      .then((response) => {
        return response as number;
      });
  }

  public createSysAdmin(login: string, firstName: string, lastName: string, middleName: string, password: string): Promise<any> {
    return this.http.post(environment.baseUrl + '/rms-authorization/api/admin', {
      login: login,
      firstName: firstName,
      lastName: lastName,
      middleName: middleName,
      password: password
    }).toPromise();
  }

  getUsers(role: any, registrationDate?: number, status?: number): Promise<UserDTO[]> {
    const params = {
      role: role
    };
    return this.http
      .post(this.userAPI + '/search', params)
      .map(response => {
        let users = response as UserDTO[];
        users = users.map(user => {
          user.registrationDate = new Date(user.registrationDate).getTime();
          return user;
        });
        return users;
      })
      .do(log => console.log(log))
      .toPromise();
  }

}
