import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from '@angular/common/http';

import { InterceptorModule } from "./_services/auth.interceptor"
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScriptLoaderService } from "./_services/script-loader.service";
import { PrimengService } from "./_services/primeng.service";
import { ModalsService } from "./_services/modals.service";
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    ThemeComponent,
    AppComponent,
  ],
  imports: [
    HttpClientModule,
    InterceptorModule,
    LayoutModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ThemeRoutingModule,
    AuthModule,
    DataTableModule,
    SharedModule,
    NgbModule.forRoot()
  ],
  providers: [ScriptLoaderService, PrimengService, ModalsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
