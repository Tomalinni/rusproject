import { Injectable } from "@angular/core";
//import { Response, Headers, } from "@angular/http";
import { HttpClient, HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpParams } from '@angular/common/http';
import "rxjs/add/operator/map";
import { Observable } from 'rxjs/Observable';

import "rxjs/add/operator/map";
import { environment } from "../../../environments/environment";
import { Router } from "@angular/router";
import "rxjs/add/operator/toPromise";

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient, private _router: Router) {
  }

  login(email: string, password: string) {
    return this
      .requestToken(new HttpParams()
        .append("grant_type", "password")
        .append("username", email)
        .append("password", password))
      .map(this.acceptToken)
    //.do(() => {
    //  //for debugging - spoil the access token
    //  sessionStorage.setItem('token', 'invalid-token');
    //})
  }

  refreshAccessToken() {
    let refreshToken: string = sessionStorage.getItem('refreshToken');
    return this
      .requestToken(new HttpParams()
        .append("grant_type", "refresh_token")
        .append("refresh_token", refreshToken))
      .map(this.acceptToken);
  }

  private requestToken(payload: HttpParams): Observable<any> {
    return this.http.post(environment.baseUrl + '/oauth/token', {}, {
      headers: new HttpHeaders()
        .append("Authorization", "Basic " + btoa("rajithapp" + ":" + "secret"))
        .append("Content-Type", "application/x-www-form-urlencoded"),
      params: payload
    });
  }

  private acceptToken(infoToken: any) {
    sessionStorage.setItem('token', infoToken.access_token);
    sessionStorage.setItem('refreshToken', infoToken.refresh_token);
    // login successful if there's a jwt token in the response
    // TODO change this.
    let user = { id: infoToken.userId, fullName: "Demo", email: "admin@11", token: "fake-jwt-token" }; //response.json();
    if (user && user.token) {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('currentUser', JSON.stringify(user));
    }
  }

  public logout() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('refreshToken');
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this._router.navigate(['/login']);
  }
}
