import { Injectable } from "@angular/core";
import { Headers, Http, Response, RequestOptions } from "@angular/http";
import { HttpHeaders, HttpClient, HttpResponse } from "@angular/common/http";

import { User } from "../_models/index";
import { Observable } from "rxjs/Observable";
import { environment } from "../../../environments/environment";
import { PrimeNgInputComponent } from "../../theme/pages/default/angular/primeng/input/primeng-input.component";
import { EditPlanArgumentsDTO } from "../../admin/templates/models/editPlanArgumentsDTO";
import { UserSearchRequest } from "../../admin/users/model/user-search-request";
import { UserModel } from "../../admin/users/model/user.model";
import { Injure } from "../../admin/templates/models/injure.model";

@Injectable()
export class UserService {
  private userResource: string = environment.baseUrl + "/rms-authorization/api/user";

  constructor(private http: HttpClient,
    private httpOld: Http /*can't wipe this out: fake-jwt uses fake-backend.ts bypassed ny new HttpClient*/) {
  }

  /*
  * Поиск пользователей по роли, статусу или по верменному промежутку даты регистрации
  */
  public findUsersByRoleStatusRegistrationDate(searchParams: UserSearchRequest): Promise<UserModel[]> {
    return this.httpOld
      .post(this.userResource + '/search'
      , searchParams
      , this.accessToken())
      .toPromise()
      .then(response => {
        return response.json() as UserModel[];
      });
  }

  getSysAdminUsers(): Promise<any> {
    return this.http.get(environment.baseUrl + '/rms-authorization/api/admin')
      .toPromise();
  }

  blockAdministrator(id, block, desc, isAbleToBeRecovered): Promise<any> {
    // TODO check id; if (){}
    return this.http.put(environment.baseUrl + '/rms-authorization/api/user/lock', {
      userId: id,
      lockReasonTypeId: block,
      lockReasonComment: desc,
      isAbleToBeRecovered: isAbleToBeRecovered
    }).toPromise();
  }

  unblockAdministrator(id): Promise<any> {
    return this.http.put(environment.baseUrl + '/rms-authorization/api/user/unlock', {
      userId: id
    }, {
        responseType: 'text'
      }).toPromise();
  }

  getBlockList(): Promise<any> {
    return this.http.get(environment.baseUrl + '/rms-authorization/api/lock-reason')
      .toPromise();
  }

  activateAdmin(login, passwd, key): Promise<any> {
    return this.http.put(environment.baseUrl + '/registration/confirm', {
      login: login,
      password: passwd,
      activationKey: key
    }).toPromise();
  }

  getInjury(): Promise<Injure[]> {
    return this.http.get(environment.baseUrl + '/rms-rehabilitation/injury')
      .toPromise();
  }

  getRehabByInjureList(injureId) {
    return this.http.get(environment.baseUrl + '/rms-rehabilitation/planTemplate/search/byInjury?injury=' + injureId)
      .toPromise();
  }


  getRehabList() {
    return this.http.get(environment.baseUrl + '/rms-rehabilitation/planTemplate/')
      .toPromise();
  }

  verify() {
    //At this point, due to "Fake jwt token" and fake-backend.ts, /verify essentially just tests whether there is
    //  a current-user or not :)
    return this.httpOld.get('/rms-authorization/api/verify', this.jwt()).map((response: Response) => response.json());
  }

  forgotPassword(email: string) {
    return this.httpOld.post('/rms-authorization/api/forgot-password', JSON.stringify({ email }), this.jwt()).map((response: Response) => response.json());
  }

  getAll() {
    return this.httpOld.get('/rms-authorization/api/users', this.jwt()).map((response: Response) => response.json());
  }

  getById(id: number) {
    //todo fix url construction
    return this.httpOld.get('/rms-authorization/api/users/' + id, this.jwt()).map((response: Response) => response.json());
  }

  create(user: User) {
    return this.httpOld.post('/rms-authorization/api/users', user, this.jwt()).map((response: Response) => response.json());
  }

  update(user: User) {
    //todo fix url construction
    return this.httpOld.put('/rms-authorization/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
  }

  delete(id: number) {
    //todo fix url construction
    return this.httpOld.delete('/rms-authorization/api/users/' + id, this.jwt()).map((response: Response) => response.json());
  }

  public saveRehabilitationPlan(planId: number, body: EditPlanArgumentsDTO): Promise<any> {
    return this.httpOld.put(environment.baseUrl + '/rms-rehabilitation/planTemplate/' + planId, body, this.accessToken()).toPromise();
  }

  private accessToken(): RequestOptions {
    let myHeaders = new Headers();
    let options = new RequestOptions({ headers: myHeaders });
    myHeaders.append("Content-Type", "application/json");
    let token = sessionStorage.getItem('token');
    myHeaders.append('Authorization', 'Bearer' + token);

    return new RequestOptions({ headers: myHeaders });
  }

  // private helper methods

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
      return new RequestOptions({ headers: headers });
    }
  }

  setBlockStatus(item) {

  }
}
