import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from "../_services/user.service";

import { Validators } from '@angular/forms';

import { NgForm } from '@angular/forms';

@Component({
  selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
  templateUrl: './accept-register.component.html',
  styles: []
})
export class AcceptRegisterComponent implements OnInit {
  model: any = {};
  /*login: string;
  password: string;
  confirm: string;*/
  key: string;
  showPassword: string;
  isShowPass: boolean = false;

  constructor(private _router: ActivatedRoute, private _user: UserService, private _routing: Router) { }



  activate() {
    this._user.activateAdmin(this.model.login, this.model.password, this.key).then(result => this._routing.navigate(['']));
  }

  ngOnInit() {
    this.showPassword = "password";
    this.key = this._router.params['_value'].id;
  }

  showHidePassword() {

    if (this.showPassword == "password") {
      this.showPassword = "text";
      this.isShowPass = true;
    }
    else {
      this.showPassword = "password";
      this.isShowPass = false;
    }
  }

}
