import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthComponent } from "./auth.component";
import { AcceptRegisterComponent } from "./accept-register/accept-register.component";

const routes: Routes = [
  { path: '', component: AuthComponent },
  { path: 'confirm/:id', component: AcceptRegisterComponent }
  /*{ path: '', component: AcceptRegisterComponent },
  { path: 'confirm/:id', component: AuthComponent }*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
