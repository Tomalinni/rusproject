import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ScriptLoaderService } from "../_services/script-loader.service";
import { AuthenticationService } from "./_services/authentication.service";
import { AlertService } from "./_services/alert.service";
import { UserService } from "./_services/user.service";
import { AlertComponent } from "./_directives/alert.component";
import { LoginCustom } from "./_helpers/login-custom";
import { Helpers } from "../helpers";
import { Response } from "@angular/http";

@Component({
  selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
  templateUrl: './login/login.component.html',
  encapsulation: ViewEncapsulation.None
})

export class AuthComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;
  showPassword: string;
  isShowPass: boolean = false;
  show: boolean;


  @ViewChild('alertSignin', { read: ViewContainerRef }) alertSignin: ViewContainerRef;
  @ViewChild('alertSignup', { read: ViewContainerRef }) alertSignup: ViewContainerRef;
  @ViewChild('alertForgotPass', { read: ViewContainerRef }) alertForgotPass: ViewContainerRef;

  constructor(private _router: Router,
    private _script: ScriptLoaderService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _authService: AuthenticationService,
    private _alertService: AlertService,
    private cfr: ComponentFactoryResolver) {
  }


  ngOnInit() {
    this.showPassword = "password";
    this.model.remember = false;
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
    this._router.navigate([this.returnUrl]); //attempt to go back from this page if there is a valid logged-in user
    this._script.load('body', 'assets/vendors/base/vendors.bundle.js', 'assets/demo/default/base/scripts.bundle.js')
      .then(() => {
        Helpers.setLoading(false);
        LoginCustom.init();
      });
  }

  showHidePassword() {

    if (this.showPassword == "password") {
      this.showPassword = "text";
      this.isShowPass = true;
    }
    else {
      this.showPassword = "password";
      this.isShowPass = false;
    }
  }

  showField(i) {
    this.show = i;
  }

  signin() {
    this.loading = true;
    this._authService.login(this.model.email, this.model.password)
      .subscribe(
      data => {
        this._router.navigate([this.returnUrl]);
      },
      error => {

        this.showAlert('alertSignin');
        this._alertService.error('Неверное имя пользователя или пароль!');
        this.loading = false;
        if (error.status === 401) { this._alertService.error('Пользователь заблокирован!'); }
      });
  }

  signup() {
    this.loading = true;
    this._userService.create(this.model)
      .subscribe(
      data => {
        this.showAlert('alertSignin');
        this._alertService.success('Thank you. To complete your registration please check your email.', true);
        this.loading = false;
        LoginCustom.displaySignInForm();
        this.model = {};
      },
      error => {
        this.showAlert('alertSignup');
        this._alertService.error(error);
        this.loading = false;
      });
  }

  forgotPass() {
    this.loading = true;
    this._userService.forgotPassword(this.model.email)
      .subscribe(
      data => {
        this.showAlert('alertSignin');
        this._alertService.success('Cool! Password recovery instruction has been sent to your email.', true);
        this.loading = false;
        LoginCustom.displaySignInForm();
        this.model = {};
      },
      error => {
        this.showAlert('alertForgotPass');
        this._alertService.error(error);
        this.loading = false;
      });
  }

  showAlert(target) {
    this[target].clear();
    let factory = this.cfr.resolveComponentFactory(AlertComponent);
    let ref = this[target].createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }
}
