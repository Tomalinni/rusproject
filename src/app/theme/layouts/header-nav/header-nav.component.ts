import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';
import { Helpers } from '../../../helpers';

declare let mLayout: any;
@Component({
  selector: "app-header-nav",
  templateUrl: "./header-nav.component.html",
  encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {


  constructor(private location: Location) {

  }
  ngOnInit() {

  }
  ngAfterViewInit() {

    mLayout.initHeader();

  }

  goBack(): void {
    this.location.back();
  }

}
