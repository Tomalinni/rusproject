
var BootstrapMaxlength = function () {

  var demos = function () {

    $('.form-control').maxlength({
      threshold: 5,
      warningClass: "m-badge m-badge--primary m-badge--rounded m-badge--wide",
      limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide",
      appendToParent: true
    });

  }

  return {
    // public functions
    init: function() {
      demos();
    }
  };
}();

jQuery(document).ready(function() {
  BootstrapMaxlength.init();
});


var Inputmask = function () {


  var demos = function () {

    $("#endEducationDate2").inputmask("dd.mm.yyyy", {
      autoUnmask: true
    });

  }

  return { // public functions
    init: function() {
      demos();
    }
  };
}();

jQuery(document).ready(function() {
  Inputmask.init();
});
