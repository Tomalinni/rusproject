//== Class definition
var dataSource;

var DatatableDataLocalDemo = function () {
  //== Private functions

  // demo initializer
  var demo = function () {

    var datatable = $('.m_datatable').mDatatable({
      // datasource definition
      data: {
        type: 'local',
        source: dataSource,
        pageSize: 10
      },

      // layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        height: 450, // datatable's body's fixed height
        footer: false // display/hide footer
      },

      // column sorting
      sortable: true,

      pagination: true,

      // inline and bactch editing(cooming soon)
      // editable: false,

      // columns definition
      columns: [{
        field: "id",
        title: "id",
        width: 50,
        sortable: false,
        selector: false,
        textAlign: 'center'
      },
        {
          field: "firstName",
          title: "ФИО",
          responsive: {visible: 'lg'}
        },
        {
          field: "creationDate",
          title: "Регистрация",
          type: "datetime",
          filter: "contains",
          template: function (row) {
            return moment(row.creationDate).format("MMMM DD.MM.YYYY  HH:mm:ss");
          }
        },
        {
          field: "statusCode",
          title: "Статус",
          // callback function support for column rendering
          template: function (row) {
            var status = {
              ACTIVE: {'title': 'Активен', 'class': ' m-badge--success'},
              NOT_ACTIVE: {'title': 'Не активен', 'class': ' m-badge--metal'},
              LOCKED : {'title': 'Заблокирован', 'class': ' m-badge--danger'},
              // 4: {'title': 'Pending', 'class': 'm-badge--brand'},
              // 5: {'title': 'Canceled', 'class': ' m-badge--primary'},
              // 6: {'title': 'Info', 'class': ' m-badge--info'},
              // 7: {'title': 'Warning', 'class': ' m-badge--warning'}
            };
            return '<span class="m-badge ' + status[row.statusCode].class + ' m-badge--wide">' + status[row.statusCode].title + '</span>';

          },
        },
        {
          field: "Actions",
          width: 110,
          title: "",
          sortable: false,
          overflow: 'visible',
          template: function (row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
						<div class="dropdown ' + dropup + '">\
							<a href="#" class="btn m-btn m-btn--hover-inverse-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                <i class="fa fa-cog"></i>\
                            </a>\
						  	<div class="dropdown-menu dropdown-menu-right">\
						    	<a class="dropdown-item" href="#"> Просмотреть профиль</a>\
						    	<a class="dropdown-item" href="#"> Заблокировать роль</a>\
						  	</div>\
						</div>\
					';
          }
        }
      ]
    });

    var query = datatable.getDataSourceQuery();

    $('#m_form_search').on('keyup', function (e) {
      datatable.search($(this).val().toLowerCase());
    }).val(query.generalSearch);

    $('#m_form_status').on('change', function () {
      datatable.search($(this).val(), 'Status');
    }).val(typeof query.Status !== 'undefined' ? query.Status : '');

    $('#m_form_type').on('change', function () {
      datatable.search($(this).val(), 'Type');
    }).val(typeof query.Type !== 'undefined' ? query.Type : '');

    $('#m_form_status, #m_form_type').selectpicker();

  };

  return {
    //== Public functions
    init: function () {
      // init dmeo
      demo();
    }
  };
}();

jQuery(document).ready(function () {
  var token = sessionStorage.getItem('token');
  var x = new XMLHttpRequest();
  x.open("GET", "http://172.29.5.18:8088/api/list/admin", true );
  x.setRequestHeader('Authorization', 'Bearer' + token + '');
  x.onload = function (){
    dataSource = JSON.parse(x.response);
    DatatableDataLocalDemo.init();
  }
  x.send();
});
